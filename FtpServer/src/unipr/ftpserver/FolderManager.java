package src.unipr.ftpserver;


import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.lang3.SystemUtils;


public class FolderManager {


	static public Path createDirectory(Path perc, String dir) throws InvalidPathException {
		perc = FileSystems.getDefault().getPath(perc.toString() + File.separator + dir);
		return createDirectory(perc);
	}


	static public Path createDirectory(Path perc) {
		try {
			Path p = Files.createDirectories(perc);
			File f = new File(p.toString());
			if (f.exists()){
				return p;
			}
			else
				throw new InvalidPathException(perc.toString(),"Can't create this path.");
		}
		catch(IOException e){
			System.out.println("Cartella '" + perc.getFileName() + "' non creata.");
			return null;
		}

	}


	static public File generateTempFile(File directory) throws IOException {
		File temp = File.createTempFile("tempfile", ".temp", directory); 
		return temp;
	}

	static public Path createFile(Path perc, String nomefile) {
		try {
			perc = FileSystems.getDefault().getPath(perc + "/" +nomefile);
			return Files.createFile(perc);
		}
		catch(IOException e){
			System.out.println("File '" + nomefile + "' non creato.");
			return null;
		}

	}

	static public String getFileName(String path){
		String[] c = path.split(Pattern.quote("\\"));
		if (c.length == 1)
			c = path.split(Pattern.quote("/"));
		if (c.length == 1)
			return c[0];
		return c[c.length - 1];
	}

	static public Path compose(Path current, String received) throws RuntimeException {
		try {
			if (received.startsWith("/") || received.startsWith("\\")){
				received = received.replace("\\", File.separator);
				received = received.replace("/", File.separator);
				return Paths.get(received);
			}
			else {
				String newPath = current.toString() + File.separator + received;
				// si accerta che si utilizzi un solo separatore: File.separator
				newPath = newPath.replace("\\", File.separator);
				newPath = newPath.replace("/", File.separator);
				return Paths.get(newPath);
			}
		}
		catch (InvalidPathException e){
			throw new RuntimeException("This file does not exist");
		}
	}

	static public boolean renameFile(Path perc, String namefile, String newnamefile){   
		File old = new File(Paths.get(perc + "/" + namefile).toString());
		if(!old.exists()){
			System.out.println("File doesn't exist.");
			return false;   
		}   
		File new_file = new File(Paths.get(perc + "/" + newnamefile).toString());
		if (new_file.exists()) new_file.delete();
		old.renameTo(new_file);   
		return true;
	}

	public static Path defaultFolder(){
		if (SystemUtils.IS_OS_WINDOWS){
			return Paths.get("C:/Users/"+ System.getProperty("user.name") + "/Documents/");
		}
		else {
			String user = System.getenv("SUDO_USER");
			if (user == null)
				user = System.getProperty("user.name");
			return Paths.get("/home/" + user);			
		}
	}


	public static boolean remove(Path path){
		File file = new File(path.toString());
		File currentFile;
		if (file.exists()){
			String[] components = file.list();
			if (components != null){
				for(String s: components){
					currentFile = new File(file.getPath(),s);
					remove(currentFile.toPath());
					currentFile.delete();
				}
				file.delete();
			}
			else
				file.delete();
		}
		return true;
	}



	public static List<String> getFileList(Path perc) throws IOException {
		List<String> files = new ArrayList<>();
		try (DirectoryStream<Path> stream = Files.newDirectoryStream(perc, "*")) {
			for (Path file : stream) { // per ogni file
				String text = file.toString();
				File f = new File(text);
				String nuova = "";
				if (f.isDirectory()){
					nuova = File.separator;	
				}
				text = getFileName(text);
				files.add(text + nuova);
			}
		}
		return files;
	}



	public static boolean contains(Path parent, Path son) throws IOException{
		File p = new File(parent.toString());
		File s = new File(son.toString());
		parent = Paths.get(p.getCanonicalPath());
		son = Paths.get(s.getCanonicalPath());
		if (son.startsWith(parent)){
			return true;
		}
		else return false;
	}


	public static void main(String[] args) throws IOException {
		Path p = Paths.get(defaultFolder().toString() + "/FtpSmart/");
		p = p.getParent();
		List<String> prova = getFileList(p);
		for (String s : prova){
			System.out.println(s);
		}
	}



}



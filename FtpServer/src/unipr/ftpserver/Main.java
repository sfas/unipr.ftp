package src.unipr.ftpserver;

import java.io.IOException;


public class Main {
	public static void main(String[] args) {
		try {
			FtpServer s = new FtpServer(21);
			s.startServer();
			s.run();
		}
		catch(IOException e){
			System.out.println("Error: ");
			e.printStackTrace();
		}
	}

}


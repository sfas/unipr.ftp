package src.unipr.ftpserver;

import java.io.File;
import java.io.IOException;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;



public class FtpServer extends Thread {
	public Path fileLog;
	
	private int port;
	private ServerSocket c;
	
	public Path filePassw;
	
	public Path path;
	
	public FtpServer(int port, Path path) {
		this.port = port;
		this.path = path; // dove viene lanciato l'FtpServer
		
	}
	
	
/////////////////////////////////////////////////////////////////////////////////////////////////////////	

	
	public FtpServer(int port) throws IOException {
		try {
				path = Paths.get(FolderManager.defaultFolder() + "/FtpSmartServer");
				path =  Files.createDirectories(path);
				System.out.println("Set base path.");
		}
		catch(IOException e){
			System.out.println("Base path is not allowed.");
			throw e;
		}
		fileLog = Paths.get(path.getFileName() + ".log");
		filePassw = Paths.get(path.getFileName() + "_passw.txt");
		this.port = port;
		createFileLog();
		createFilePassw();
		LogManager.getInstance().log(Level.INFO, "FtpServer created on path " + path + ".\n");
	}

	
/////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	
	public FtpServer(Path dir, int port){
		// quello sotto: comando per ottenere la parent del punto in cui viene lanciato il server
		// path = Paths.get(FtpServer.class.getProtectionDomain().getCodeSource().getLocation().getPath().substring(1)).getParent();;
		path = dir;
		fileLog = Paths.get(path.getFileName() + ".log");		
		filePassw = Paths.get(path.getFileName() + "_passw.txt");
		this.port = port;
		createFileLog();
		createFilePassw();
	}
	
///////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	public FtpServer(Path dir, Path log, Path pass, int port){
		// quello sotto: comando per ottenere la parent del punto in cui viene lanciato il server
		// path = Paths.get(FtpServer.class.getProtectionDomain().getCodeSource().getLocation().getPath().substring(1)).getParent();;
		path = dir;
		fileLog = log;		
		filePassw = pass;
		this.port = port;
		createFileLog();
		createFilePassw();
	}
	
///////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	public void startServer() throws IOException {
			c = new ServerSocket(port);	
	}
	
	
/////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	public void run()
		{
			while(true){
				try{
					Socket s = c.accept();
					LogManager.getInstance().log(Level.INFO,s.getInetAddress() + " port " + s.getPort() + " is connected."  + "\n");
					System.out.println(s.getInetAddress() + " port " + s.getPort() + " is connected.");
					createControl(s,path);		
				}
				catch(Throwable e){
					System.out.println("Thrown an exception: " + e.toString());
				}
			}
		}
	
	
/////////////////////////////////////////////////////////////////////////////////////////////////////////	

	
	private void createControl(Socket s, Path path){
		ControlThread t = new ControlThread(s, path);
		t.start();
	}

	
/////////////////////////////////////////////////////////////////////////////////////////////////////////	

	
	private void createFileLog(){
		try {
			
			LogManager.setInstance(fileLog);
			System.out.println("Created log in " + fileLog + "\n");
		}
		catch(IOException e){
			System.err.println("IOException in creation of " + fileLog);
		}
		catch (SecurityException e){
			System.err.println("SecurityException in creation of " + fileLog);
		}
	}

/////////////////////////////////////////////////////////////////////////////////////////////////////////	

	
	private void createFilePassw(){
		try {
			FolderManager.createDirectory(path.getParent());
			FolderManager.createFile(filePassw.getParent(),filePassw.getFileName().toString());
			File pass = filePassw.toFile();
			UserManager.setInstance(pass);
			LogManager.getInstance().log(Level.FINE,"Created password file in " + filePassw + "\n");
			System.out.println("Created password file in " + filePassw + "\n");
		}
		catch (SecurityException e){
			LogManager.getInstance().log(Level.SEVERE,"SecurityException in creation of " + filePassw + "\n");
			System.out.println("SecurityException in creation of " + filePassw);
		}
	}
	
	///////////////////////////////////////////////////////////////
	
	public void exit() throws IOException {
		c.close();
	}
	
}

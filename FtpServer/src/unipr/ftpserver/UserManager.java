package src.unipr.ftpserver;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jasypt.util.password.StrongPasswordEncryptor;

public class UserManager {
	public StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
	public File passwFile;
	// necessario per poter usare la classe in maniera statica, si user� sempre questo UserManager
	public static UserManager instance;


	public static void setInstance(File passw){
		instance = new UserManager();
		instance.passwFile = passw;
	}


	public static UserManager getInstance(){
		if (instance == null){
			return null;
		}
		else
			return instance;

	}

	public boolean checkPresence(String ut) throws FileNotFoundException {
		String temp = "";
		ut = ut.toUpperCase();
		Scanner s = new Scanner(passwFile);
		boolean presence = false; // per individua l'esistenza dell'utente con nickname dato
		// controllo di ogni riga del file
		while (s.hasNextLine()){
			temp = s.nextLine();
			String[] tokens = temp.split(":");
			if (tokens[0].equals(ut)){
				presence = true;
				temp = tokens[0];
				break;
			}
		}
		s.close();
		return presence;
	}

	//////////////////////////////////////////////////////////////////////////


	public boolean addUser(String ut, String pass) throws FileNotFoundException{	
		// creazione file autenticazione se gia' non esiste
		// path = createDirectory(path);
		// autentication = createFile(path,filePassw).toFile();

		// controllo non esista gia' utente
		ut = ut.toUpperCase();
		pass = pass.trim();
		if (!checkPresence(ut)){
			// accertata assenza, creata password criptata da inserire
			String eP = passwordEncryptor.encryptPassword(pass);
			try{
				byte[] testo = ((ut + ":" + eP + '\n')).getBytes();
				Files.write(passwFile.toPath(), testo, StandardOpenOption.APPEND);
				// logger.info("Created user " + ut + ".\n");
				return true;
			}
			catch(Throwable e){
				// logger.info("Error, new user won't be added.\n");
				return false;
			}
		}
		else
			return false;
	}

	/////////////////////////////////////////////////////////////////////////////////////////

	public void removeUser(String ut){
		try { 
			ut = ut.toUpperCase();
			String name = passwFile.getCanonicalPath();
			System.out.println(name);
			Files.createFile(Paths.get(name + ".temp"));
			File tempFile = new File(name + ".temp");
			BufferedReader br = new BufferedReader(new FileReader(passwFile));
			PrintWriter pw = new PrintWriter(new FileWriter(tempFile));
			String currentLine = "";
			ut = ut.trim();

			while ((currentLine = br.readLine()) != null){
				currentLine = currentLine.trim();
				if (! currentLine.startsWith(ut)){
					pw.write(currentLine + System.getProperty("line.separator"));
				}
			}
			br.close();
			pw.close();
			passwFile.delete();
			tempFile.renameTo(new File(name));
			LogManager.getInstance().log(Level.INFO, "Removed from passwFile user: " + ut);
		}
		catch(IOException e){
			LogManager.getInstance().log(Level.SEVERE, "Error: is not possible to remove " + ut);
			//	return false;
		}
	}

	/////////////////////////////////////////////////////////////////////////////////////////


	public boolean loginUser(String ut, String pass){
		try { 
			ut = ut.toUpperCase();
			String temp;
			Scanner s = new Scanner(passwFile);
			while (s.hasNextLine()){
				temp = s.nextLine();
				String[] tokens = temp.split(":");
				temp = temp.substring(tokens[0].length() +1);
				if (tokens[0].equals(ut)){
					if (passwordEncryptor.checkPassword(pass, temp)) {
						s.close();
						return true;
					}
					else {
						s.close();
						return false;
					}
				}
			}	
			s.close();
			return false;
		}
		catch(Throwable e){
			e.printStackTrace();
			return false;
		}
	}
}

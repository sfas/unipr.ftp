package src.unipr.ftpserver;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Level;
import java.util.regex.Pattern;

public class CommandManager {
	ControlThread control;
	String oldfile;

	public CommandManager(ControlThread c){
		control = c;

	}


	public String parseCommand(String cmd) throws IOException{
		String[] c = cmd.split(Pattern.quote(" "));
		String resto = cmd.substring(c[0].length()).trim();
		switch(c[0].toUpperCase()){
		case "MKD": {
			return mkd(resto);
		}
		case "RMD": return rmd(resto);
		case "LIST": {if (c.length == 1)
			return list("");
		else return list(resto);
		}
		case "RNTO": return rnto(resto);
		case "RNFR": return rnfr(resto);
		case "USER": return user(resto);
		case "PASS": return pass(resto);
		case "CDUP": return cdup();
		case "PORT": return port(resto);
		case "CWD" : return cwd(resto);
		case "PASV": return pasv();
		case "SIZE": return size(resto);
		case "STOR": return stor(resto);
		case "RETR":  return retr(resto);
		case "DELE": return dele(resto);
		case "NOOP": return noop();
		case "PWD": return pwd();
		case "ASCII":return "220 operazione completata";
		case "BINARY": return "220 operazione completata";
		case "TYPE": return type(resto);
		case "": return "";
		case "QUIT":
		case "BYE":   quit();
		return "";
		default:
			if(c[0].charAt(0) != 10) {
				return new String("502 Command not implemented.");
			}
			return "";
		}
	}


	public String type(String a){
		if (a.toLowerCase().equals("a")) return "200 Type set to A.";
		return "500 Failed.";
	}
	
	public String port(String port) throws IOException, RuntimeException{
		if (!control.convalidated) return new String("530 Not logged in.");
		if (port.contains(",")) port = findPort(port);
		if (port == "") return new String("501 Port is not set.");
		LogManager.getInstance().log(Level.FINEST, new String("PORT command sent from " + control.id  + "\n"));
		try {
			int p = Integer.parseInt(port);
			if (p > 1024 && p <= 65535){
				control.clientPort = p;
				if (control.tt != null) control.tt.interrupt();
				control.tt = null;
				control.passive = false;
				control.tt = new TransfertThread(control);
				control.tt.start();
				return new String("220 Action has been successfully completed.");
			}
			return new String("520 Port is not allowed.");
		}
		catch(Throwable e){
			LogManager.getInstance().log(Level.WARNING, "Port: format error");
			e.printStackTrace();
			return new String("520 The sent port is not a number.");
		}
	}

	public String findPort(String mex) {
		String[] c = mex.split(Pattern.quote(","));
		if (c.length != 6) return "";
		String ad;
		try {
			ad = new String (Integer.parseInt(c[0]) +".");
			ad = new String (ad + Integer.parseInt(c[1]) +".");
			ad = new String (ad + Integer.parseInt(c[2]) +".");
			ad = new String (ad + Integer.parseInt(c[3]));
			int porta = Integer.parseInt(c[4]) * 256 + Integer.parseInt(c[5]);
			return new String("" + porta);
		}
		catch(NumberFormatException e){
			LogManager.getInstance().log(Level.WARNING, "Port: format error");
			e.printStackTrace();
			return new String("");
		}
	}


	public String cdup() throws IOException{
		if (!control.convalidated) return new String("530 Not logged in.");
		if (FolderManager.contains(control.basePath, control.currentPath)){
			Path p=control.currentPath.getParent();
			if(FolderManager.contains(control.basePath, p)){
				control.currentPath=p;
				return new String("250 operazione completata, remote file system");
			}
		}
		return new String("450 operazione fallita, remote file system");
	}


	public String cwd(String dir) throws IOException{
		File f;
		// indirizzo assoluto
		if (!(dir.startsWith("\\") || dir.startsWith("/"))){
			if (!control.convalidated) return new String("530 Not logged in.");
			if(FolderManager.contains(control.basePath, control.currentPath)){
				Path p = Paths.get(control.currentPath.toString());
				try {
					p = p.resolve(dir);
					f = new File(p.toString());
					if (!f.exists()) return new String("450 operazione fallita.");
					if (!f.isDirectory()) f = f.getParentFile();
				}
				catch (InvalidPathException e){
					return new String("450 operazione fallita.");
				}	
				if(FolderManager.contains(control.basePath,f.toPath())){
					control.currentPath = f.toPath();
					return new String("250 operazione completata.");	
				}
				else return new String("450 operazione fallita.");
			}
			else return new String("450 operazione fallita.");
		}
		else { // indirizzo relativo
			Path b = Paths.get(control.basePath.toString() + dir);
			if(FolderManager.contains(control.basePath, b)){
				Path p = control.basePath;
				try {
					p = b.resolve("");
					f = new File(p.toString());
					if (!f.exists()) return new String("450 operazione fallita.");
					if (!f.isDirectory()) f = f.getParentFile();
				}
				catch (InvalidPathException e){
					return new String("450 operazione fallita.");
				}	
				if(FolderManager.contains(control.basePath,f.toPath())){
					control.currentPath = f.toPath();
					return new String("250 operazione completata.");	
				}
				else return new String("450 operazione fallita.");
			}
			else return new String("450 operazione fallita.");
		}

	}

	public String size(String filename){
		if (!control.convalidated) return new String("530 Not logged in.");
		try {
			Path interestedPath = Paths.get(control.currentPath + File.separator + filename);
			if (! FolderManager.contains(control.basePath, interestedPath)) return new String("NO OK");
			File f = new File(interestedPath.toString());
			if(f.exists()){
				long size = f.length();
				return new String("OK " + String.valueOf(size) + " bytes");
			}
			else return new String("450 operazione fallita, remote file system");
		}
		catch(IOException e){
			return new String("450 operazione fallita, remote file system");
		}
	}

	public String mkd(String c1){
		if (!control.convalidated) return new String("530 Not logged in.");
		try {
			Path newPath = Paths.get(control.currentPath.toString(), File.separator, c1);
			if (FolderManager.contains(control.basePath, newPath)){
				FolderManager.createDirectory(control.currentPath, c1);
				LogManager.getInstance().log(Level.FINE, "Created directory " + c1 + "in path " + control.currentPath  + "\n");
				return new String("250 Operazione completata, remote file system");
			}
			else
				LogManager.getInstance().log(Level.WARNING, new String(control.id + " tried to create a folder in a non valide path: " + control.currentPath + File.separator + c1  + "\n"));
			return new String("450 operazione fallita.");
			// DA AGGIUNGERE RISPOSTA DEL SERVER, quella sopra è TEMPORANEA
		}
		catch(IOException e){
			return new String("450 operazione fallita.");
			// DA AGGIUNGERE RISPOSTA DEL SERVER, quella sopra è TEMPORANEA
		}
	}

	public String rmd(String c1){
		if (!control.convalidated) return new String("530 Not logged in.");
		try {
			if (FolderManager.contains(control.basePath, control.currentPath)){
				Path p = Paths.get(control.currentPath.toString(), File.separator, c1);
				File f = p.toFile();
				if (!f.exists()) return "450 operazione fallita.";
				FolderManager.remove(p);
			}
			else{
				LogManager.getInstance().log(Level.WARNING, new String(control.user + " tried to remove folder" + c1 + " in path: " + control.currentPath  + "\n"));
				
			}
			return new String("250 Operazione completata");
			// DA AGGIUNGERE RISPOSTA DEL SERVER, quella sopra e' TEMPORANEA
			// break;	
		}
		catch(IOException e){
			return new String("450 operazione fallita");
			// DA AGGIUNGERE RISPOSTA DEL SERVER, quella sopra e' TEMPORANEA
		}
	}


	public String rnto(String c1){

		if (!control.convalidated ) return new String("530 Not logged in.");
		if(oldfile=="")return new String("450 operazione fallita.");

		try {
			if (!FolderManager.contains(control.basePath, Paths.get(control.currentPath + File.separator + oldfile)))
				return new String("450 operazione fallita, remote file system");
			if (!FolderManager.contains(control.basePath, Paths.get(control.currentPath + File.separator + c1)))
				return new String("450 operazione fallita, remote file system");
			FolderManager.renameFile(control.currentPath, oldfile, c1);
			LogManager.getInstance().log(Level.FINEST, new String("RENAME sent from " + control.id  + "\n"));
			oldfile="";
			return new String("250 Operazione completata.");
		}
		catch(IOException e){
			return new String("450 operazione fallita.");
		}
	}

	public String rnfr(String c1) throws IOException{
		if (!control.convalidated) return new String("530 Not logged in.");
		File f;
		if (FolderManager.contains(control.basePath, control.currentPath)){
			if (!(c1.startsWith("\\") || c1.startsWith("/")))
				f =  new File(control.currentPath + File.separator + c1);
			else
				f = new File(control.basePath + File.separator + c1);
			if(f.exists()){
				oldfile=c1;
				return new String("250 Operazione completata, remote file system");
			}			
			return new String("450 operazione fallita, remote file system");
		}
		return new String("450 operazione fallita, remote file system");

	}

	public String pwd(){
		if (!control.convalidated) return new String("530 Not logged in.");
		try {
			if (FolderManager.contains(control.basePath, control.currentPath)){
				File f = new File(control.currentPath.toString());
				control.currentPath = Paths.get(f.getCanonicalPath());
				File ff = new File(control.basePath.toString());
				control.basePath = Paths.get(ff.getCanonicalPath());
				Integer lungh = control.currentPath.toString().length() - control.basePath.toString().length();
				if (lungh == 0) return "257" + " \"\\\"";
				return "257" + " \""+ control.currentPath.toString().substring(control.currentPath.toString().length() - lungh)+ "\"";
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new String("450 operazione fallita.");
	}


	public String dele(String f) throws IOException{
		if (!control.convalidated) return new String("530 Not logged in.");
		if (FolderManager.contains(control.basePath, control.currentPath)){
			Path p1 = Paths.get(control.currentPath.toString()+ File.separator + f);
			if (!FolderManager.contains(control.basePath, p1)) return new String("550 operazione fallita, no permesssi.");
			LogManager.getInstance().log(Level.FINEST, new String("DELETE sent from " + control.id  + "\n"));
			if(FolderManager.remove(p1)) return new String ("250 Operazione completata, remote file system");
			return new String("550 operazione fallita.");
		}
		return new String("550 operazione fallita, no permessi.");
	}


	public String user(String name){
		LogManager.getInstance().log(Level.FINEST, new String("USER command sent from " + control.id  + "\n"));
		if (name.contains(":")) return new String("530 operazione fallita, USER/PASS");
		else {
			control.user = name;
			control.convalidated = false;
			return new String("331, Password required.");
		}
		// DA AGGIUNGERE RISPOSTA DEL SERVER, quella sopra e' TEMPORANEA
		// break;
	}

	public String pass(String pass){
		LogManager.getInstance().log(Level.WARNING, new String("PASS command sent from " + control.id  + "\n"));
		UserManager um = UserManager.getInstance();
		if (um != null){
			control.convalidated = um.loginUser(control.user, pass);
			if (control.convalidated){
				control.id = "user " + control.user + " at " + control.s.getInetAddress() + ": " + control.s.getPort();
				return new String("230 User logged in.");
			}
			else
				return new String("530 Not logged in.");
		}
		else
			return new String("530 Not logged in.");
	}

	public String noop(){
		if (!control.convalidated) return new String("530 Not logged in.");
		return new String("220 operazione completata");
	}

	public String quit(){
		try {
			control.in.close();
			control.out.close();
			control.sout.close();
			control.s.close();
			control.interrupt();
			return new String("221 Goodbye");
		} catch (IOException e) {
			System.out.println("Error in quit.");
			e.printStackTrace();
			return new String ("221 Goodbye");
		}
	}

	// upload del client
	public String stor(String nomeFile) throws UnknownHostException, IOException{
		if (!control.convalidated) return new String("530 Not logged in.");
		try {
			// prende il solo nome del file richiesto
			nomeFile = FolderManager.getFileName(nomeFile);
			Path composto;
			try {
				composto = FolderManager.compose(control.currentPath, nomeFile);
			}
			catch(Throwable e){
				//TODO controlla codice
				return new String("550 This file doesn't exist.");
			}
			if (FolderManager.contains(control.basePath, composto)){
				if (control.tt != null){
					control.tt.stor(composto);
					LogManager.getInstance().log(Level.FINEST, new String("STOR "+ composto + "sent from " + control.id  + "\n"));
					return new String ("220 Operation completed.");
				}
				else return "500 Send me PAVS or PORT before."; //TODO controllo codice
			}
			else
				// se non si ha accesso alla cartella, non in base path.
				return new String("550 Not possible to upoload in this path.");
		}
		catch(RuntimeException e){
			return e.getMessage();
		}
		catch(Throwable e){
			control.tt.interrupt();
			control.tt = null;
			//TODO controllare codice
			return new String("425 Error in creatind data connection");
		}
	}

	// download del client
	public String retr(String nomeFile) throws UnknownHostException, IOException{
		if (!control.convalidated) return new String("530 Not logged in.");
		try {
			nomeFile = nomeFile.trim();
			Path p;
			if (nomeFile.startsWith("\\") || nomeFile.startsWith("/")){
				// perch� � un indirizzo assoluto
				try {
					p = Paths.get(nomeFile);
				}
				catch (InvalidPathException e){
					return "550 This file doesn't exist";
				}
			}
			else
				try {
					p = FolderManager.compose(control.currentPath, nomeFile);
				}
			catch(Throwable e){
				//TODO controlla codice
				return new String("500 This file doesn't exist.");
			}
			// se non � contenuto nel pathbase, non pu� scaricare il file
			if (FolderManager.contains(control.basePath, p)){
				File n = p.toFile();
				if (!n.exists()){
					// il file non esiste e quindi non pu� scaricarlo
					return new String("550 Not possible to download this file.");
				}
				if (control.tt != null){
					control.tt.retr(p);
					return new String ("220 Operation completed.");
				}
				else return "500 Send me PAVS or PORT before."; //TODO controllo codice

			}
			else
				return new String("550 Not possible to download in this remote folder.");
		}
		catch(RuntimeException e){
			return e.getMessage();
		}
		catch(Throwable e){
			if (control.tt != null)
				control.tt.interrupt();
			return new String("425 Error creating data connection.");
		}
	}

	public String list(String directory){
		if (!control.convalidated) return new String("530 Not logged in.");
		try {
			// creo il file da mandare
			File temp = FolderManager.generateTempFile(control.basePath.getParent().toFile());
			Path p;
			// perch� � un indirizzo assoluto
			if (directory.startsWith("\\") || directory.startsWith("/")){
				try {
					p = Paths.get(directory);
				}
				catch (InvalidPathException e){
					return "550 This directory doesn't exist.";
				}
			} // per indirizzo relativo
			else {
				try {
					p = FolderManager.compose(control.currentPath, directory);
				}
				catch(Throwable e){
					return new String("550 This directory doesn't exist.");
				}
			}

			// se non � contenuto nel pathbase, non pu� scaricare il file
			if (FolderManager.contains(control.basePath, p)){
				File n = p.toFile();
				if (!n.exists()){
					// il file non esiste e quindi non pu� scaricarlo
					return new String("550 Directory doesn't exist.");
				}
				if (!n.isDirectory()){
					// ls lo usi solo su directory. Se non la � non va bene.
					return new String("550 Not a directory.");
				}
				// scrittura del file
				List<String> ls = FolderManager.getFileList(p);
				PrintWriter writer = new PrintWriter(temp, "UTF-8");
				for (String file : ls){
					writer.println(file);
				}
				writer.close();

				if (control.tt != null){
					control.tt.list(temp.toPath());
					FolderManager.remove(temp.toPath());
					return new String ("220 Operation completed.");
				}
				else return "500 Send me PAVS or PORT before."; //TODO controllo codice
			}
			else return "500 This file is not avaible.";

		}
		catch(RuntimeException e){
			if (control.tt != null) control.tt.interrupt();
			control.tt = null;
			return new String("503 Send me PORT command first.");
		}
		catch(Throwable e){
			if (control.tt != null) control.tt.interrupt();
			control.tt = null;
			return new String("425 Error creating data connection.");
		}
	}

	public String pasv(){
		if (!control.convalidated) return new String("530 Not logged in.");
		if (control.tt != null) control.tt.interrupt();
		control.tt = null;
		control.passive = true;
		try {
			control.tt = new TransfertThread(control);
			control.tt.start();
		} catch (IOException e) {
			e.printStackTrace();
			return "500 error in creation on socket";
		}
		InetAddress host = control.tt.ssock.getInetAddress();
		int porta = control.tt.ssock.getLocalPort();
		try {
			String hh = createPort(host,porta);
			return "227 Entering Passive Mode " + hh;
		}
		catch(Throwable e){
			e.printStackTrace();
			return e.getMessage();
		}
	}

	public String createPort(InetAddress host, int port) throws UnknownHostException{
		InetAddress address = InetAddress.getLocalHost(); 
	    String hostIP = address.getHostAddress() ;
		String[] h = hostIP.split(Pattern.quote("."));
		int quoz = port / 256;
		int resto = port - quoz*256; 
		if (h.length != 4) throw new RuntimeException("500 problem in resolution InetAdress");
		return new String ("("+ h[0] + ", " + h[1] + ", " + h[2] + ", " + h[3] + ", " + quoz + ", " + resto + ")");
	}
}


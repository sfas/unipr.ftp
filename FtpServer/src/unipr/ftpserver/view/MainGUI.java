package src.unipr.ftpserver.view;



import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import src.unipr.ftpserver.FolderManager;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class MainGUI extends Application {
	private File p;
	private File l;
	private Path d;
	private Stage st;

	@Override
	public void start(Stage primaryStage) {
		st = primaryStage;
		GridPane grid = new GridPane();
		grid.setPadding(new Insets(20));
		grid.setVgap(5);
		grid.setHgap(5);

		Text dir = new Text("Directory:");
		grid.add(dir, 0, 0);

		final TextField dirtext = new TextField();
		dirtext.setPrefColumnCount(15);
		grid.add(dirtext, 1, 0);

		Button bdir = new Button("Browse");
		grid.add(bdir, 2, 0);

		Text passw = new Text("Password File:");
		grid.add(passw, 0, 1);

		final TextField passtext = new TextField();
		passtext.setPrefColumnCount(15);
		grid.add(passtext, 1, 1);

		Button bpassw = new Button("Browse");
		grid.add(bpassw, 2, 1);

		Text log = new Text("Log File:");
		grid.add(log, 0, 2);

		final TextField logtext = new TextField();
		logtext.setPrefColumnCount(15);
		grid.add(logtext, 1, 2);

		Button blog = new Button("Browse");
		grid.add(blog, 2, 2);
		
		Text lport = new Text("Port:");
		grid.add(lport, 0, 3);

		final TextField porttext = new TextField();
		porttext.setPrefColumnCount(15);
		porttext.setPromptText("21");
		grid.add(porttext, 1, 3);

		Button ok = new Button("Confirm");

		passtext.setPromptText(FolderManager.defaultFolder() + File.separator + "FtpSmartServer" + File.separator + "PasswSmartFtp.txt");
		logtext.setPromptText(FolderManager.defaultFolder() + File.separator + "FtpSmartServer" + File.separator + "LogSmartFtp.log");
		dirtext.setPromptText(FolderManager.defaultFolder() + File.separator + "FtpSmartServer" + File.separator + "Shared Folder" + File.separator);

		EventHandler<KeyEvent> connectOnEnter = new EventHandler<KeyEvent>(){
			@Override
			public void handle(KeyEvent event){
				if (dirtext.getText().isEmpty())
					dirtext.setText(FolderManager.defaultFolder() + File.separator + "FtpSmartServer" + File.separator + "Shared Folder" + File.separator);
				if (logtext.getText().isEmpty())
					logtext.setText(FolderManager.defaultFolder() + File.separator + "FtpSmartServer" + File.separator + "LogSmartFtp.log");
				if (passtext.getText().isEmpty())
					passtext.setText(FolderManager.defaultFolder() + File.separator + "FtpSmartServer" + File.separator + "PasswSmartFtp.txt");
				File file = Paths.get(dirtext.getText()).toFile();
				if (!file.exists()){
					FolderManager.createDirectory(file.toPath());
					file = Paths.get(passtext.getText()).toFile();
				}
				if (!file.isDirectory()){
					System.out.println(file.toString());
					error("Error","The first field must be a directory");
					return;
				}
				file = Paths.get(passtext.getText()).toFile();
				if (!file.exists()){
					FolderManager.createFile(Paths.get(file.getParent()), file.getName());
					file = Paths.get(passtext.getText()).toFile();
				}
				if (file.isDirectory()){
					System.out.println(file.toString());
					error("Error","The second field mustn't be a directory");
					return;
				}
				file = Paths.get(logtext.getText()).toFile();
				if (!file.exists()){
					FolderManager.createFile(Paths.get(file.getParent()), file.getName());
					file = Paths.get(passtext.getText()).toFile();
				}
				if (file.isDirectory()){
					System.out.println(file.toString());
					error("Error","The third field must be a directory");
					return;
				}
				if (porttext.getText().isEmpty())
					porttext.setText("21");
				int portnum;
				try {
					portnum = Integer.parseInt(porttext.getText());
				} catch(Throwable t) {
					portnum = -1;
				}
				if(portnum < 0 || portnum > 65535){
					error("This port is not allowed","Error");
					return;
				}
					
				try {
					ServerManager sm= new ServerManager(Paths.get(dirtext.getText()),Paths.get(logtext.getText()),Paths.get(passtext.getText()),portnum);
					st.close();
					sm.start(st);
				}
				catch(IOException e){
					System.out.println("Error. ");
					e.printStackTrace();
				}
			}
		};
		
		dirtext.setOnKeyPressed(connectOnEnter);
		passw.setOnKeyPressed(connectOnEnter);
		logtext.setOnKeyPressed(connectOnEnter);
		lport.setOnKeyPressed(connectOnEnter);
		
		
		bdir.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event){
				Stage stage = new Stage();
				DirectoryChooser dc = new DirectoryChooser();
				d = dc.showDialog(stage).toPath();
				if (d != null)
					dirtext.setText(d.toString());
			}
		});

		bpassw.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event){
				Stage stage = new Stage();
				FileChooser dc = new FileChooser();
				p = dc.showSaveDialog(stage);
				if (p != null)
					passtext.setText(p.toString());
			}
		});

		blog.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event){
				Stage stage = new Stage();
				FileChooser dc = new FileChooser();
				l = dc.showSaveDialog(stage);
				if (l != null)
					logtext.setText(l.toString());
			}
		});


		
		
		ok.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event){
				if (dirtext.getText().isEmpty())
					dirtext.setText(FolderManager.defaultFolder() + File.separator + "FtpSmartServer" + File.separator + "Shared Folder" + File.separator);
				if (logtext.getText().isEmpty())
					logtext.setText(FolderManager.defaultFolder() + File.separator + "FtpSmartServer" + File.separator + "LogSmartFtp.log");
				if (passtext.getText().isEmpty())
					passtext.setText(FolderManager.defaultFolder() + File.separator + "FtpSmartServer" + File.separator + "PasswSmartFtp.txt");
				File file = Paths.get(dirtext.getText()).toFile();
				if (!file.exists()){
					FolderManager.createDirectory(file.toPath());
					file = Paths.get(passtext.getText()).toFile();
				}
				if (!file.isDirectory()){
					System.out.println(file.toString());
					error("Error","The first field must be a directory");
					return;
				}
				file = Paths.get(passtext.getText()).toFile();
				if (!file.exists()){
					FolderManager.createFile(Paths.get(file.getParent()), file.getName());
					file = Paths.get(passtext.getText()).toFile();
				}
				if (file.isDirectory()){
					System.out.println(file.toString());
					error("Error","The second field mustn't be a directory");
					return;
				}
				file = Paths.get(logtext.getText()).toFile();
				if (!file.exists()){
					FolderManager.createFile(Paths.get(file.getParent()), file.getName());
					file = Paths.get(passtext.getText()).toFile();
				}
				if (file.isDirectory()){
					System.out.println(file.toString());
					error("Error","The third field must be a directory");
					return;
				}
				if (porttext.getText().isEmpty())
					porttext.setText("21");
				int portnum;
				try {
					portnum = Integer.parseInt(porttext.getText());
				} catch(Throwable t) {
					portnum = -1;
				}
				if(portnum < 0 || portnum > 65535){
					error("This port is not allowed","Error");
					return;
				}
					
				try {
					ServerManager sm= new ServerManager(Paths.get(dirtext.getText()),Paths.get(logtext.getText()),Paths.get(passtext.getText()),portnum);
					st.close();
					sm.start(st);
				}
				catch(IOException e){
					System.out.println("Error. ");
					e.printStackTrace();
				}
			}
		});

		BorderPane bp = new BorderPane();
		bp.setPadding(new Insets(10));
		bp.setCenter(grid);
		BorderPane b = new BorderPane();
		b.setCenter(ok);
		bp.setBottom(b);
		bp.centerProperty();

		Scene scene = new Scene(bp);
		st.getIcons().add(new Image("file:icons/FreeVector-World-Logo.png",250,250,true,true));
		st.setTitle("Server Ftp");
		st.setScene(scene);
		st.show();
	}
	public static void main(String[] args) {
		launch(args);
	}

	public void error(String messaggio,String titolo){
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle(messaggio);
		alert.setHeaderText(null);
		alert.setContentText(titolo);
		final Stage stage1;
		stage1 = (Stage)alert.getDialogPane().getScene().getWindow();
		alert.showAndWait();
	}
}
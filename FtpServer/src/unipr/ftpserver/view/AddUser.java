package src.unipr.ftpserver.view;

import java.io.FileNotFoundException;
import java.io.IOException;

import src.unipr.ftpserver.UserManager;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class AddUser extends Application {
	private Stage primaryStage;
	private Scene principalScene;
	
	@Override
	public void start(Stage primaryStage) throws IOException {
		this.primaryStage=primaryStage;
		this.primaryStage.setTitle("FTP Server");
		this.primaryStage.getIcons().add(new Image("file:icons/FreeVector-World-Logo.png",250,250,true,true));
		this.primaryStage.setResizable(true);

		MainStructure();
	}
	
	public AddUser(){
	}
	
	private void MainStructure() {

		VBox vBox = new VBox();
		vBox.setMaxWidth(150);
		vBox.setPadding(new Insets(5));
		
		primaryStage.getIcons().add(new Image("file:icons/FreeVector-World-Logo.png",250,250,true,true));
		primaryStage.setTitle("Add User - Ftp Server");
		
		Label usernameLabel = new Label("USERNAME");
		Label passwordLabel = new Label("PASSWORD");
		final TextField username = new TextField();
		final PasswordField password = new PasswordField();
		password.setPrefWidth(125.0);
		username.setPrefWidth(125.0);
		Button connect = new Button("Add User");
		
		EventHandler<KeyEvent> connectOnEnter = new EventHandler<KeyEvent>(){
			@Override
			public void handle(KeyEvent ke){
				if (ke.getCode().equals(KeyCode.ENTER)){
					add(username.getText(),password.getText());
					exit();
				}
			}
		};
		
		username.setOnKeyPressed(connectOnEnter);
		password.setOnKeyPressed(connectOnEnter);
		connect.setOnKeyPressed(connectOnEnter);

		vBox.getChildren().addAll(usernameLabel,username,passwordLabel,password,connect);

		BorderPane br = new BorderPane();
		br.setPadding(new Insets(20));
		br.setCenter(vBox);
		BorderPane br1 = new BorderPane();
		br1.setCenter(connect);
		br.setBottom(br1);
		
		
		connect.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (!username.getText().isEmpty()){
				add(username.getText(),password.getText());
				exit();
				}
				else
					error("Username is empty.","Empty Fields");
			}
		});
		
		principalScene = new Scene(br);
		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
	          public void handle(WindowEvent we) {
	              try {
					exit();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	          }
	      });  
		
		
		this.primaryStage.setScene(principalScene);
		this.primaryStage.show();

	}
	

	public boolean add(String ut,String pass) {
		if (ut.contains(":")){
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("User not accepted, you can't use the character ':'.");
			final Stage stage;
			stage = (Stage)alert.getDialogPane().getScene().getWindow();
			stage.getIcons().add(new Image("file:icons/FreeVector-World-Logo.png"));
			alert.showAndWait();
		}
		else {
			try {
				UserManager.getInstance().addUser(ut, pass);
				try {
					exit();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (FileNotFoundException e) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Error");
				alert.setHeaderText(null);
				alert.setContentText("File not found.");
				final Stage stage;
				stage = (Stage)alert.getDialogPane().getScene().getWindow();
				stage.getIcons().add(new Image("file:icons/FreeVector-World-Logo.png"));
				alert.showAndWait();
			}
		}
		boolean successed = UserManager.getInstance().loginUser(ut, pass);
		return successed;
	}

	private void exit() {
		primaryStage.close();
		try {
			this.stop();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void error(String messaggio,String titolo){
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle(messaggio);
		alert.setHeaderText(null);
		alert.setContentText(titolo);
		final Stage stage1;
		stage1 = (Stage)alert.getDialogPane().getScene().getWindow();
		alert.showAndWait();
	}


	
}

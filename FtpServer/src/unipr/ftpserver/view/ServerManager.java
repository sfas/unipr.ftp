package src.unipr.ftpserver.view;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Optional;

import src.unipr.ftpserver.FtpServer;
import src.unipr.ftpserver.UserManager;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class ServerManager extends Application {
	private Stage primaryStage = new Stage();
	private Scene principalScene;
	private FtpServer s;

	public ServerManager(final Path dir, final Path log, final Path pass, final int port) throws IOException{

		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				try {
					s = new FtpServer(dir,log,pass,port);
					s.startServer();
					s.start();
				} catch (IOException e) {
					error("Error","Impossible to use this configuration.");
					primaryStage.close();
					MainGUI mg = new MainGUI();
					Stage st = new Stage();
					mg.start(st);
				}
			}
		});


	}

	@Override
	public void start(Stage primaryStage) throws IOException {
		this.primaryStage=primaryStage;
		this.primaryStage.setTitle("FTP Server");
		this.primaryStage.getIcons().add(new Image("file:icons/FreeVector-World-Logo.png",250,250,true,true));
		this.primaryStage.setResizable(true);

		MainStructure();

	}

	private void MainStructure() {

		VBox vBox = new VBox();
		vBox.setPrefWidth(160);

		Button add = new Button("Add user");
		Button del = new Button("Delete user");
		Button change = new Button("Change password");
		Button exit = new Button("Exit");

		add.setMinWidth(vBox.getPrefWidth());
		del.setMinWidth(vBox.getPrefWidth());
		change.setMinWidth(vBox.getPrefWidth());
		exit.setMinWidth(vBox.getPrefWidth());

		vBox.getChildren().addAll(add,del,change,exit);

		BorderPane br = new BorderPane();
		br.setPadding(new Insets(18));
		br.setCenter(vBox);
		
		exit.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				exit();
			}
		});
		
		del.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				RemoveUser a = new RemoveUser();
				Stage st = new Stage();
				try {
					a.start(st);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		add.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				AddUser a = new AddUser();
				Stage st = new Stage();
				try {
					a.start(st);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		change.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				ChangePassw a = new ChangePassw();
				Stage st = new Stage();
				try {
					a.start(st);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		principalScene = new Scene(br);
		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
	          public void handle(WindowEvent we) {
	              exit();
	          }
	      });  
		
		this.primaryStage.setScene(principalScene);
		this.primaryStage.show();

	}





	public void error(String messaggio,String titolo){
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle(messaggio);
		alert.setHeaderText(null);
		alert.setContentText(titolo);
		final Stage stage1;
		stage1 = (Stage)alert.getDialogPane().getScene().getWindow();
		alert.showAndWait();
	}

	public void exit(){
		try {
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Confirm Exit");
			alert.setHeaderText(null);
			alert.setContentText("Are you sure you want to exit?");
			final Stage stage;
			stage = (Stage)alert.getDialogPane().getScene().getWindow();
			stage.getIcons().add(new Image("file:icons/FreeVector-World-Logo.png"));
			Optional<ButtonType> result = alert.showAndWait();
			if(result.get() == ButtonType.OK){
				s.exit();
				System.exit(0);}
		} catch (IOException e) {
			error("Error in closing FtpServer.","Error");
		}
	}
	
	public void stop(){
		exit();
	}
	
	public String dialog(String title,String message){
		TextInputDialog dialog = new TextInputDialog();
		dialog.setTitle(title);
		dialog.setContentText(message);
		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()){
		    return result.get();
		}
		else return null;
	}
}

package src.unipr.ftpserver;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Path;


public class TransfertThread extends Thread {

	public Socket sock = null;
	public ServerSocket ssock = null;
	BufferedInputStream bis;
	FileInputStream fis;
	OutputStream os;
	FileOutputStream fos; 
	boolean passive;
	ControlThread control;
	String action;

	// solo se la modalit� � passiva serve la porta, altrimenti la calcola
	public TransfertThread(ControlThread c) throws IOException, RuntimeException {
		control = c;
		if (c.passive == false){
			if (control.clientPort == 0) throw new RuntimeException("500 Port must be different from zero.");
			}
		else {
			ssock = new ServerSocket(0);
		}
	}


	public String stor(Path p){
		try {
			if (passive == false) sock = new Socket(control.s.getInetAddress(),control.clientPort);
			else sock = ssock.accept();
			receive(p);
		}
		catch(Throwable e){
			return ("500 " + e.getMessage());
		}
		return "220 action completed.";
	}

	public String retr(Path p){
		try {
			if (passive == false) sock = new Socket(control.s.getInetAddress(),control.clientPort);
			else sock = ssock.accept();
			send(p);
		}
		catch(Throwable e){
			return ("500 " + e.getMessage());
		}
		return "220 action completed.";
	}

	public String list(Path p){
		try {
			if (passive == false) sock = new Socket(control.s.getInetAddress(),control.clientPort);
			else sock = ssock.accept();
			send(p);
		}
		catch(Throwable e){
			return ("500 " + e.getMessage());
		}
		return "220 action completed.";
	}

	//per passare la stringa del path dove si trova il file
	public void send(Path perc){
		try {
			File f = new File(perc.toString());
			byte [] mybytearray  = new byte [1024];
			fis = new FileInputStream(f);
			bis = new BufferedInputStream(fis);
			os = sock.getOutputStream();
			int count;
			while ((count = bis.read(mybytearray)) > 0){
			    os.write(mybytearray, 0, count);
			    os.flush();
			    String datastring = new String(mybytearray, "UTF-8");
			    System.out.println(datastring);
			}	
		}
		catch (Throwable e) {
			e.printStackTrace();
			throw new RuntimeException("Error in send.");
		}
		try {
			bis.close();
			os.close();
			sock.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("Error in close send.");
		}
	}

	public void receive(Path perc){
		try {
			byte [] mybytearray  = new byte [1024];		
			bis = new BufferedInputStream(sock.getInputStream());
			fos = new FileOutputStream(perc.toString());
			int count;
			while ((count = bis.read(mybytearray)) > 0)
			{
			    fos.write(mybytearray, 0, count);
			    fos.flush();
			    String datastring = new String(mybytearray, "UTF-8");
			    System.out.println(datastring);
			}
		}
		catch (Throwable e) {
			e.printStackTrace();
			throw new RuntimeException("Error in receive.");
		}
		try {
			bis.close();
			fos.close();
			sock.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("Error in close receive.");
		}
	}
}






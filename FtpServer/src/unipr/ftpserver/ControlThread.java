package src.unipr.ftpserver;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.file.Path;
import java.util.logging.Level;

public class ControlThread extends Thread {
	public Socket s= new Socket();

	public String user = "";
	public boolean convalidated = false;
	public String id;

	public Path basePath;
	public Path currentPath;

	public int port;
	public int serverPort = 20;
	public int clientPort;
	public InetAddress addr;
	public boolean passive = false;
	public boolean stor = false;

	public CommandManager commandManager; 

	public String namePasswFile;

	public InputStream in;
	public byte[] inputMessage = new byte[255];
	public OutputStream sout;
	public PrintWriter out;

	public TransfertThread tt;


	public ControlThread(){
		id = s.getInetAddress().toString() + ":" + s.getPort();
	}

	public void run(){
		try {
			String received;
			String tosend;

			in = s.getInputStream();
			LogManager.getInstance().log(Level.FINER, "Created input stream with " + id + "\n");

			sout = s.getOutputStream();
			LogManager.getInstance().log(Level.FINER, "Created output stream with " + id + "\n");

			LogManager.getInstance().log(Level.FINER, "Created print writer with " + id + "\n");
			out = new PrintWriter(sout, true);
			out.println("220 WELCOME.");
			out.flush();
			while(true){
				if (in.read(inputMessage) == -1){
					quit();
					break;
				}
				received = new String(inputMessage);
				received = received.trim();
				for (int i = 0; i < inputMessage.length; i++){
					inputMessage[i] = 0;
				}
				LogManager.getInstance().log(Level.FINER, "Received string " + received + " from " + id + "\n" );
				// System.out.println(received);
				tosend = commandManager.parseCommand(received);
				out.println(tosend);
				out.flush();
				LogManager.getInstance().log(Level.FINER, "Choosen answer to " + id + ": "+ tosend + "\n");
			}

		}
		catch(IOException e){
			LogManager.getInstance().log(Level.FINER, "Error in stream with " + id + "\n");
			// System.out.println("Error in stream with " + id);
		}
	}

	public ControlThread(Socket s, Path path){
		this.s = s;
		addr = s.getInetAddress();
		basePath = path;
		currentPath = path;
		commandManager = new CommandManager(this);
		id = "user " + user + " at " + s.getInetAddress() + ": " + s.getPort();
		LogManager.getInstance().log(Level.FINEST, "Created controll thread with " + id + "\n");
		System.out.println("Created controll thread with " + id + "\n");
	}


	public void quit(){
		LogManager.getInstance().log(Level.WARNING, new String("Closed connection: " + id  + "\n"));
		try {
			in.close();
			out.close();
			sout.close();
			s.close();

		} catch (IOException e) {
			System.out.println("Error in quit.");
			e.printStackTrace();
		}
	}

}

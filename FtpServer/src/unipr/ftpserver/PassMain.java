package src.unipr.ftpserver;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;


public class PassMain {
	static Scanner in;
	static boolean quit = false;

	public static void main(String[] args) {
		in = new Scanner(System.in);
		System.out.println("What's the password file?");
		Path path =  FolderManager.defaultFolder();
		boolean correct = false;
		File passw = new File(FolderManager.defaultFolder().toString());
		while(!correct){
			try {
				if (in.hasNextLine()){
					path = Paths.get(in.nextLine());
					correct = true;
					passw = new File(path.toString());}
			}
			catch(InvalidPathException | NullPointerException e){
				System.out.println("This file is not a file.\nWhat's the password file?");
			}
		}	
		UserManager.setInstance(passw);


		System.out.println("What's the log file?");
		Path path1 = FolderManager.defaultFolder();
		boolean correct1 = false;
		while(!correct1){
			try {
				if (in.hasNextLine()){
					path1 = Paths.get(in.nextLine());
					correct1 = true;}
			}
			catch(InvalidPathException e){
				System.out.println("This file is not a file.\nWhat's the log file?");
			}
		}
		try {
			LogManager.setInstance(path1);
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		String op;
		while(!quit){ 
			System.out.println("What action do you want to do? add | change passw | remove | quit");
			if (in.hasNextLine()){
				op = (in.nextLine());
				if (op.equals("add")) add();
				else if (op.equals("remove")) remove();
				else if (op.equals("change passw")) changepassw();
				else if (op.equals("quit")) quit();
			}
		}
		in.close();
	}


	public static boolean add() {
		System.out.println("Who do you want to add?");
		String ut = "";
		boolean points = true;
		while (points == true){
			if (in.hasNextLine()){
				ut = in.nextLine();
				if (ut.contains(":")){
					// log.info("User not accepted, you can't use the character ':'.\n");
					System.out.println("User not accepted, you can't use the character ':'.\n");
					points = true;
				}
				else
					points = false;
			}
		}
		System.out.println("Password:");
		String pass = in.nextLine();
		try {
			UserManager.getInstance().addUser(ut, pass);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		boolean successed = UserManager.getInstance().loginUser(ut, pass);
		// System.out.println("Do the passwords match? " + successed);
		return successed;

		// System.out.println("Utente " + ut + " esistente? " + UserManager.getInstance().checkPresence(ut));
		// LoginManager.fl.close();


	}

	public static boolean remove(){

		System.out.println("Trying to remove a user. Who do you want to remove? ");
		if (in.hasNextLine()){
			String user = in.nextLine();
			boolean existing;
			try {
				existing = UserManager.getInstance().checkPresence(user);
				if (!existing) return false;
				UserManager.getInstance().removeUser(user);
				return true;
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return false;
	}


	public static void changepassw(){
		System.out.println("User:");
		String ut = "";
		boolean points = true;
		while (points){
			if (in.hasNextLine()){
				ut = in.nextLine();
				if (ut.contains(":")){
					// log.info("User not accepted, you can't use the character ':'.\n");
					System.out.println("User not accepted, you can't use the character ':'.\n");
					points = true;
				}
				else
					points = false;
			}
		}
		System.out.println("New password:");
		String Newpass = in.nextLine();
		try {
			UserManager.getInstance().removeUser(ut);
			UserManager.getInstance().addUser(ut, Newpass);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void quit(){
		quit = true;
	}

}

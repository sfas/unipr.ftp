\documentclass{article}
\usepackage[utf8x]{inputenc} % Per gli accenti
\usepackage[italian]{babel} % Per apice e pedice
\usepackage[numbered]{mcode} % Per i codici con le keyword di MATLAB
\usepackage{graphicx} % Per le immagini
\usepackage{float} % Per forzare le immagini nel posto richiesto
\usepackage{url}  % per gli indirizzi internet
\usepackage{longtable}
\usepackage{array}
\usepackage{hyperref}
\usepackage{enumitem}
\setlist[itemize]{noitemsep, topsep=0pt,leftmargin=*}
\newcommand{\specialcell}[2][c]{%
  \begin{tabular}[#1]{@{}c@{}}#2\end{tabular}}


\begin{document}

\title{\textbf{FTP Client e Server in Java\\}}
\author{Balzani Silvia, Benini Francesco, Cecja Andia, Parra Stefano}
\date{Anno accademico 2014/2015}
\
\setcounter{tocdepth}{2}%Comando per inserire l'indice.

 \textsc{\huge{\textbf{Università degli studi di Parma }\\ \large{\textbf{\hspace*{14mm}Dipartimento di Matematica e Informatica}}}}
{\let\newpage\relax\maketitle}
\vspace{20mm}

\clearpage
\tableofcontents %Posto dell'indice.
\clearpage
\section{Introduzione}

Il progetto qui presentato si propone di creare un server ed un client FTP, implementando una parte dei molti comandi standardizzati da RFC959\\ (\href{https://www.ietf.org/rfc/rfc959.txt}{https://www.ietf.org/rfc/rfc959.txt}.\\
\`{E} possibile reperire il sorgente del progetto sulla piattaforma GitLab al seguente indirizzo: \href{https://gitlab.com/sfas/unipr.ftp.git}{https://gitlab.com/sfas/unipr.ftp.git}). 

\section{Tecnologie utilizzate}
Questo progetto è stato sviluppato utilizzando:
\begin{itemize}
\item Java, unito alle librerie standard che il linguaggio mette a disposizione; 
\item JavaFX, per la parte grafica del Server e Client;
\item La libreria \textit{common-lang3.jar}, per permettere l'identificare di cartelle default su tutti i sistemi operativi;
\item La libreria \textit{jasypt-1.9.2.jar}, per criptare i file delle password.
\end{itemize}

\section{Modularizzazione del progetto}
Scopo del seguente progetto è approfondire le conoscenze sulla programmazione ed implementazione di applicativi Java che richiedano uso di socket. Si è inoltre deciso di utilizzare un'interfaccia grafica più avanzata, JavaFx, così da rendere più accattivante il programma all'utente.\\
L'argomento che in particolare si sposa con Reti dei Calcolatori riguarda la realizzazione di un coordinamento tra server e client che gestiscano un servizio per il trasferimento di file da remoto (FTP – File Transfer Protocol). In questo modo è possibile suddividere il lavoro in tre punti ben distinti:\\
\begin{itemize}
\item Realizzazione di un FTP server;
\item Realizzazione di un FTP client in grado di interagire col server FTP;
\item Realizzazione della rappresentazione grafica.\\
\end{itemize}


Particolare importanza ha avuto però l'FTP Server, il quale, avevamo deciso, sarebbe stato robusto ed avrebbe dovuto coprire una buona parte dei comandi dello standard.


\section{File Transfer Protocol}
FTP è un protocollo per la trasmissione di dati tra host basato su TCP/IP (Transmission Control Protocol/Internet Protocol) e  con architettura di tipo client-server.
FTP rappresenta uno dei primi protocolli usati nelle reti basate su TCP/IP e in Internet. Sebbene il World Wide Web abbia rimpiazzato la maggior parte delle funzioni di FTP, quest’ultimo continua ad essere un ottimo sistema per traferire file da un computer client ad un server su Internet. \`{E} però importante tenere in considerazione che il protocollo FTP non usa alcun metodo di cifratura, pertanto non è consigliabile l'utilizzo dei questa tecnologia nella condivisione di dati sensibili.\\
FTP utilizza due connessioni separate, quella di controllo e quella dati. Un server FTP generalmente rimane in ascolto sulla porta 21 a cui si connetterà il client. La connessione da parte del client determina l'inizializzazione del canale di controllo attraverso il quale il client ed il server si scambiano comandi e risposte ai rispettivi comandi. Lo scambio effettivo di dati, la cui richiesta passa per il canale del controllo, richiede l'apertura di un canale dati, che può essere di due tipi: attivo o passivo.\\
In un canale dati di tipo \emph{attivo} il client apre una porta e tramite il canale di controllo rende noto il numero di tale porta al server e attende che questo si connetta. Una volta che il server avrà attivato la connessione dati al client FTP, quest'ultimo effettuerà il binding della porta sorgente alla porta 20 del server FTP.\\
In un canale dati di tipo \emph{passivo}, invece, il server apre una porta (superiore alla 1023) e tramite il canale di controllo la rende nota al client, infine si mette in attesa che questo si connetta.\\

\begin{figure}[H]
\centerline{\includegraphics[scale=0.6]{img/attivoepassivo.png}}
\end{figure}

Importante specificare che in FTP si crea un nuovo canale dati per ogni file trasferito all'interno della sessione utente, in altre parole il canale di trasferimento non è persistente. Al contrario, il canale di controllo rimane aperto per l'intera durata della sessione utente, sarà pertanto un canale persistente.\\

\section{FTP Server}
FTP nel corso degli anni si è sempre più ingrandito, portando sempre a nuove aggiunge. Motivo per cui solo alcuni dei comandi sono stati implementati nella realizzazione del nostro server FTP. In particolare abbiamo voluto implementare comandi di base che ci permettessero di toccare con mano ciò che avviene effettivamente durante la comunicazione fra server e client.\\
\`{E} stato necessario stabilire quali fossero i comandi da riprodurre nelle prime fasi del progetto. Non solo dovevamo stabilire i comandi che il server avrebbe accettato, ma anche le risposte che il server avrebbe dato al client.\\
Qui sotto riportiamo i comandi che il server da noi realizzato accetta:\\


    \begin{longtable}{| p{.15\textwidth} | p{.40\textwidth} | p{.35\textwidth} |} 
    \hline Comando & Risposta server & Descrizione \\ \hline
    
    USER \textit{ut} &
    \textit{530}, se \textit{ut} contiene ':'. \textit{331}, per richiedere la password. &
    Permette di inizializzare il login al server indicando il nome utente \textit{ut}. 
    \\ \hline
    
    PASS \textit{password} &
    \textit{230}, se il login avviene con successo. \textit{530}, se il login è fallito.&
    Permette di completare il login al server indicando la password relativa all'utente precedentemente indicato.
    \\ \hline
    
    MKD \textit{dir} &
   \textit{250}, creazione avvenuta con successo. \textit{450}, file non disponibile. \textit{530}, non è stato effettuato il login. &
    Permette di creare una cartella di nome \textit{dir} nella directory corrente.
    \\ \hline
    
    CWD \textit{dir} &
    \textit{250}, operazione completata. \textit{450}, file non disponibile. \textit{530}, non è stato effettuato il login. &
    Spostamento dalla cartella corrente a quella specificata (accettati sia indirizzi relativi che assoluti rispetto alla radice della cartella condivisa).
    \\ \hline
    
    LIST \textit{dir} &
    \textit{250}, operazione completata. \textit{450}, file non disponibile. \textit{530}, non è stato effettuato il login. &
    Permette i file e le cartelle contenuti nella cartella indicata, o in quella corrente se non viene indicata nessuna cartella.
    \\ \hline    
    
    RMD \textit{dir} &
    \textit{250}, operazione completata. \textit{450}, file non disponibile. \textit{530}, non è stato effettuato il login. &
    Rimuove la directory specificata.
    \\ \hline 
    
    RNFR \textit{old} &
    \textit{250}, operazione completata. \textit{450}, file non disponibile. \textit{530}, non è stato effettuato il login. &
    Specifica il file che si vuole rinominare. Il comando deve essere seguito dal comando RNFR.
    \\ \hline  
    
    RNTO \textit{new} &
    \textit{250}, operazione completata. \textit{450}, file da rinominare non disponibile. \textit{530}, non è stato effettuato il login. &
    Comando successivo a RNFR, specifica il nuovo nome del file.
    \\ \hline
    
    CDUP &
    \textit{250}, operazione completata. \textit{450}, file da rinominare non disponibile. \textit{530}, non è stato effettuato il login. &
    La nuova cartella corrente diventa la cartella genitore della attuale cartella corrente.
    \\ \hline 
    
	PORT \textit{nport} &
    \textit{220}, porta assegnata con successo. \textit{530}, non è stato effettuato il login. \textit{501}, porta non specificata. \textit{520}, porta non permessa.&
    Permette di impostare la porta \textit{nport} su cui si creerà il socket di trasferimento dati.
    \\ \hline    
    
	PASV &
    \textit{227}, passaggio a modalità passiva, inoltre si riceverà nel messaggio una combinazione di \textit{(h1,h2,h3,h4,p1,p2)} che rappresenterà l'indirizzo e la porta su cui il server si metterà in ascolto. \textit{530}, non è stato effettuato il login. \textit{500}, errore nella creazione del socket. &
    Permette di passare alla modalità passiva. Il server, se tutto andrà a buon fine, risponderà specificando su quale indirizzo e porta si metterà in ascolto.
    \\ \hline    
    
	STOR \textit{file} &
    \textit{220}, operazione completata. \textit{500}, errore generico nello stor (probabilmente dovuto alla porta non indicata o alla modalità non specificata). \textit{450}, path o file indicato non permesso o esistente. \textit{530}, non è stato effettuato il login. \textit{425}, errore nella creazione della data connection. &
    Il client carica il file specificato che risiede nel disco locale nella cartella corrente del remoto.
    \\ \hline    
    
    RETR \textit{file} &
    \textit{220}, operazione completata. \textit{500}, errore generico nel retr (probabilmente dovuto alla porta non indicata o alla modalità non specificata). \textit{450}, path o file indicato non permesso o esistente. \textit{530}, non è stato effettuato il login. \textit{425}, errore nella creazione della data connection. &
    Il client riceve il file specificato che risiede sul server nella cartella che egli indica nel suo disco locale.
    \\ \hline
    
    DELE \textit{file} &
    \textit{220}, operazione completata. \textit{450}, path o file indicato non permesso o esistente. \textit{530}, non è stato effettuato il login. &
    Permette di eliminare il file indicato sul server.
    \\ \hline

	NOOP &
    \textit{220}, operazione completata. \textit{530}, non è stato effettuato il login. &
    Non fa niente.
    \\ \hline 
    
    PWD &
    \textit{257}, operazione completata, di seguito il path corrente. \textit{530}, non è stato effettuato il login. \textit{450}, path o file indicato non permesso o esistente.&
    Ritorna la cartella corrente in cui ci si trova nel server tenendo come radice la cartella condivisa.
    \\ \hline   
    
    ASCII &
    \textit{200}, operazione completata. &
    Passaggio a modalità ASCII.
    \\ \hline
    
    BINARY &
    \textit{200}, operazione completata. &
    Passaggio a modalità binary.
    \\ \hline
    
    TYPE \textit{mode}  &
    \textit{200}, operazione completata, settato nuovo tipo. \textit{500}, operazione fallita.&
    Permette di settare il il tipo ad ASCII se riceve in input la modalità ASCII, ossia \textit{A}.
    \\ \hline
    
    QUIT &
    \textit{221}, uscita eseguita con successo. &
    Uscita.
    \\ \hline
    
    BYE &
    \textit{221}, uscita eseguita con successo. &
    Uscita.
    \\ \hline
    
    \hline
    \end{longtable}

È stata implementata un'interfaccia per il Server, la quale permette di scegliere su che file eseguire il log, su quale file appoggiarsi per l'autenticazione (nel file verranno salvati gli utenti insieme alle password cifrate), che porta utilizzare (in genere si utilizza la 21) e quale cartella condividere.\\

\begin{figure}[H]
\centerline{\includegraphics[scale=0.6]{img/server1.png}}
\end{figure}

Una volta stabilito ciò il server partirà e sarà possibile tramite una nuova finestra aggiungere utenti, rimuoverli o cambiare la loro password. Da lì sarà inoltre possibile fermare il server.

Il server in particolare sfrutta il multithread, permettendo così di accettare molte richieste e gestire ogni singola connessione. Stabilita la connessione si aprirà un altro thread per permettere il trasferimento dati qualora venga richiesto.

\begin{figure}[H]
\centerline{\includegraphics[scale=0.6]{img/server2.png}}
\end{figure}

\section{FTP Client}

Il client da noi realizzato si presenta all'utente con un'interfaccia grafica JavaFX. Per accedere al server sarà necessario indicare l'host, il nome utente e la password. Opzionalmente sarà possibile indicare anche la porta, in quanto questa ha come valore di default la porta 21.\\

\begin{figure}[H]
\centerline{\includegraphics[scale=0.6]{img/client1.jpg}}
\end{figure}

Una volta compiuto il login, si avrà accesso alla directory condivisa.

\begin{figure}[H]
\centerline{\includegraphics[scale=0.6]{img/client2.jpg}}
\end{figure}

In particolare alcune azioni saranno consentite una volta entrati: aggiornare, caricare un file, scaricare un file, rinominare un file o cartella, cancellare un file oppure creare una nuova cartella.\\

Punto critico è stato permettere la gestione della TreeView permettendo di mappare gli oggetti presenti sul server senza ricorrere ad informazioni dei file, in quanto non presenti sul client. Tramite l'oggetto selezionato si è infatti studiato un metodo per ricreare il path presente sul server e mandare così il comando (o i comandi) che l'operazione primitiva del client richiede.

\section{Possibili sviluppi futuri}
\begin{itemize}
\item Permettere al client di interagire col server anche in modalità passiva.
\item Implementare ulteriori modalità di trasferimento sia su server che su client.
\item Possibilità di gestione più cartelle condivise usando una sola istanza del programma.
\item Introduzione nel comando abort.

\end{itemize}
 \qquad

\end{document}

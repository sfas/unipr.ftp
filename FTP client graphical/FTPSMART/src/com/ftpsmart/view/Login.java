package com.ftpsmart.view;

import com.ftpsmart.implementation.*;

import java.net.Socket;
import java.util.Optional;

import com.ftpsmart.implementation.FtpSmartClient;
import com.sun.javafx.applet.ExperimentalExtensions;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Login extends Application {

	private Stage primaryStage;
	private GridPane root;
	private FirstView firstview;
	private TextField host;
	private TextField port;
	private TextField username;
	private PasswordField password;
	private Button connect;
	private Button cancel;
	private Label hostLabel;
	private Label portLabel;
	private Label usernameLabel;
	private Label passwordLabel;
	private Scene firstScene;

	private VBox fieldBox;
	private HBox buttonBox;
	private HBox hostPanel;
	private HBox usernamePanel;
	private HBox passwordPanel;
	private VBox logoBox;



	@Override	
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("FTPSmart");
		this.primaryStage.setResizable(true);
		struttura();
	}


	public static void main(String[] args) {
		launch(args);
	}

	public Login() {}

	public void struttura(){
		root = new GridPane();
		hostLabel = new Label("HOST");
		portLabel = new Label("PORT");
		usernameLabel = new Label("USERNAME");
		passwordLabel = new Label("PASSWORD");
		// Setting logo iniziale
		Image logo = new Image("file:icons/FTPSMART.png",400,200,true,true);
		ImageView logoView = new ImageView();
		logoView.setImage(logo);
		// logoBox nella VBox
		logoBox = new VBox(30);
		logoBox.getChildren().add(logoView);
		logoBox.setAlignment(Pos.TOP_CENTER);
		// Setting icona a sinistra
		primaryStage.getIcons().add(new Image("file:icons/FreeVector-World-Logo.png",250,250,true,true));
		primaryStage.setTitle("FTPSmart - Login");
		primaryStage.setResizable(false);
		host = new TextField();
		port = new TextField();
		username = new TextField();
		password = new PasswordField();
		// inizializzo i textfield
		username.setPromptText("Insert your username here");
		host.setPromptText("Insert domain here");
		password.setPromptText("Insert your password here");
		port.setPromptText("21");
		// prefissata la dimensione dei textField
		host.setPrefWidth(165.0);
		password.setPrefWidth(165.0);
		username.setPrefWidth(165.0);
		port.setPrefWidth(40.0);
		//Bottoni
		connect = new Button("Connect");
		cancel = new Button("Exit");
		// aggiungo tutto ai HBox e VBox
		hostPanel = new HBox(10);
		hostPanel.getChildren().addAll(hostLabel,host,portLabel,port);
		hostPanel.setSpacing(40.0);
		hostPanel.setAlignment(Pos.CENTER_LEFT);
		usernamePanel = new HBox(10);
		usernamePanel.getChildren().addAll(usernameLabel,username);
		passwordPanel = new HBox(10);
		passwordPanel.getChildren().addAll(passwordLabel,password);
		fieldBox = new VBox(10);
		fieldBox.getChildren().addAll(logoBox,hostPanel,usernamePanel,passwordPanel);
		buttonBox = new HBox(10);
		buttonBox.getChildren().addAll(connect,cancel);
		buttonBox.setAlignment(Pos.CENTER);
		buttonBox.setSpacing(50.0);
		// aggiungi tutto al GridPane
		root.add(logoBox, 0, 0);
		root.add(fieldBox, 0, 1);
		root.add(buttonBox, 0, 2);
		root.setAlignment(Pos.CENTER);
		root.setHgap(20);
		root.setVgap(25);
		root.setPadding(new Insets(25,25,25,25));

		// Tutte le ActionListener del LoginInterface
		EventHandler<KeyEvent> connectOnEnter = new EventHandler<KeyEvent>(){
			@Override
			public void handle(KeyEvent ke){
				if (ke.getCode().equals(KeyCode.ENTER)){
					connect();
				}
			}
		};

		username.setOnKeyPressed(connectOnEnter);
		password.setOnKeyPressed(connectOnEnter);
		port.setOnKeyPressed(connectOnEnter);
		host.setOnKeyPressed(connectOnEnter);
		connect.setOnKeyPressed(connectOnEnter);

		cancel.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Alert alert = new Alert(AlertType.CONFIRMATION);
				alert.setTitle("Confirm Exit");
				alert.setHeaderText(null);
				alert.setContentText("Are you sure you want to exit?");
				Optional<ButtonType> result = alert.showAndWait();
				if(result.get() == ButtonType.OK)
					System.exit(0);
			}
		});

		cancel.setOnKeyPressed(new EventHandler<KeyEvent>(){
			@Override
			public void handle(KeyEvent ke){
				if (ke.getCode().equals(KeyCode.ENTER)){
					Alert alert = new Alert(AlertType.CONFIRMATION);
					alert.setTitle("Confirm Exit");
					alert.setHeaderText(null);
					alert.setContentText("Are you sure you want to exit?");
					Optional<ButtonType> result = alert.showAndWait();
					if(result.get() == ButtonType.OK)
						System.exit(0);
				}
			}
		});

		connect.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event){
				connect();
			}
		});

		// Show everything
		firstScene = new Scene(root,450,425);
		this.primaryStage.setScene(firstScene);
		this.primaryStage.show();
	}

	final public void connect(){
		int portnum;
		try {
			portnum = Integer.parseInt(port.getText());
		} catch(Throwable t) {
			portnum = -1;
		}

		if (host.getText().isEmpty() || username.getText().isEmpty()){
			Alert alertconnect = new Alert(AlertType.ERROR);
			alertconnect.setTitle("Empty fields");
			alertconnect.setHeaderText(null);
			alertconnect.setContentText("Please fill the required fields.");
			alertconnect.showAndWait();
		}
		else if (port.getText().isEmpty()){
			port.setText("21");
			portnum = 21;
			try {
				Socket s = new Socket(host.getText(), portnum);
				firstview = new FirstView();
				CommandManager cm = new CommandManager(s, username.getText(), password.getText(), firstview);
				firstview.addCommandManager(cm);
				firstview.start(primaryStage);
			}
			catch(Exception e){
				e.printStackTrace();
				Alert alertconnect = new Alert(AlertType.ERROR);
				alertconnect.setTitle("Socket expection");
				alertconnect.setHeaderText(null);
				alertconnect.setContentText(e.getMessage());
				alertconnect.showAndWait();
			}
		}


		else if(portnum < 0 || portnum > 65535){

			Alert alertconnect = new Alert(AlertType.ERROR);
			alertconnect.setTitle("Port Error");
			alertconnect.setHeaderText(null);
			alertconnect.setContentText("Port is out of range.");
			alertconnect.showAndWait();
		}

		else {
			try {
				Socket s = new Socket(host.getText(), portnum);
				firstview = new FirstView();
				CommandManager cm = new CommandManager(s, username.getText(), password.getText(), firstview);
				firstview.addCommandManager(cm);
				firstview.start(primaryStage);
			}
			catch(Exception e){
				e.getStackTrace();
				Alert alertconnect = new Alert(AlertType.ERROR);
				alertconnect.setTitle("Socket expection");
				alertconnect.setHeaderText(null);
				alertconnect.setContentText("Error in comunication with server.");
				alertconnect.showAndWait();
			}

		}
	}

}

package com.ftpsmart.view;
import com.ftpsmart.implementation.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.sound.midi.Receiver;
import javax.swing.text.html.Option;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TreeView.EditEvent;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import javafx.util.Callback;
import javafx.util.converter.DefaultStringConverter;

public class FirstView extends Application {
	private Stage primaryStage;
	private Stage aboutStage;

	private Login loginshow;

	public CommandManager cm;

	private StackPane aboutPane;
	private Scene principalScene;
	private BorderPane grid;
	private BorderPane mainLayout;

	//private VBox local;
	private BorderPane server;
	private HBox bottoni;
	private BorderPane percbox;
	private BorderPane inizio;

	private MenuBar menuBar;
	private Menu menu;
	private Menu help;

	private MenuItem about;
	private MenuItem refresh;
	private MenuItem disconnetti;
	private MenuItem esci;
	private Label info;
	private Label percorsoServer;

	public TreeView<String> albero;
	public TreeItem<String> ramo;
	public Button aggiorna;
	private Button carica;
	private Button scarica;
	private Button cancella;
	private Button nuovadir;
	private Button rinominaB;



	@Override
	public void start(Stage primaryStage) {
		this.primaryStage=primaryStage;
		this.primaryStage.setTitle("FTPSmart");
		this.primaryStage.setResizable(true);
		MainStructure();

		//newDirectory();

	}

	public void addCommandManager(CommandManager cm) {
		this.cm = cm;
	}
	public FirstView(){

	}

	private void MainStructure() {
		// icona a sinistra
		primaryStage.getIcons().add(new Image("file:icons/FreeVector-World-Logo.png",250,250,true,true));
		primaryStage.setTitle("FTPSmart");

		// Barra dei Men�
		menuBar = new MenuBar();
		menu = new Menu("Menu");
		help = new Menu("Help");

		// ELEMENTI da aggiungere al Men�
		refresh = new MenuItem("Refresh");
		disconnetti = new MenuItem("Disconnect");
		esci = new MenuItem("Exit");
		about = new MenuItem("Info");

		// AGGIUNTA al men�
		menu.getItems().addAll(refresh,disconnetti,esci);
		help.getItems().add(about);
		menuBar.getMenus().addAll(menu,help);
		menuBar.setStyle("-fx-background-color:transparent ;");

		//Bottoni in alto a destra
		bottoni = new HBox();
		aggiorna = new Button();
		carica = new Button();
		scarica = new Button();
		cancella = new Button();
		nuovadir = new Button();
		rinominaB = new Button();

		aggiorna.setTooltip(new Tooltip("Refresh"));
		carica.setTooltip(new Tooltip("Upload"));
		scarica.setTooltip(new Tooltip("Download"));
		cancella.setTooltip(new Tooltip("Delete"));
		nuovadir.setTooltip(new Tooltip("New directory"));
		rinominaB.setTooltip(new Tooltip("Rename"));

		Image im = new Image("file:icons/refresh.png",20,20,true,true);
		Image ima = new Image("file:icons/upload.png",20,20,true,true);
		Image imag = new Image("file:icons/download.png",20,20,true,true);
		Image newd = new Image("file:icons/create_directory.png",20,20,true,true);
		Image image = new Image("file:icons/delete.png",20,20,true,true);
		Image ren = new Image("file:icons/rename.png",20,20,true,true);

		aggiorna.setGraphic(new ImageView(im));
		carica.setGraphic(new ImageView(ima));
		scarica.setGraphic(new ImageView(imag));
		nuovadir.setGraphic(new ImageView(newd));
		cancella.setGraphic(new ImageView(image));
		rinominaB.setGraphic(new ImageView(ren));

		aggiorna.setStyle("-fx-background-color: transparent;");
		carica.setStyle("-fx-background-color: transparent;");
		scarica.setStyle("-fx-background-color: transparent;");
		rinominaB.setStyle("-fx-background-color: transparent;");
		nuovadir.setStyle("-fx-background-color: transparent;");
		cancella.setStyle("-fx-background-color: transparent;");

		bottoni.getChildren().addAll(aggiorna,carica,scarica,rinominaB,cancella,nuovadir);

		// HBOX contenente parte in alto: il men� bar e i bottoni
		inizio = new BorderPane();
		inizio.setLeft(menuBar);
		inizio.setRight(bottoni);
		// inizio.setMaxSize(600, 25);

		// HANDLER DEL MENU' BAR
		// se azionato refresh
		refresh.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				try {
					cm.refresh();
				} catch (ClassNotFoundException | IOException  e) {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Error in refresh");
					alert.setHeaderText(null);
					alert.setContentText("Error in refresh. Please disconnect.");
					final Stage stage;
					stage = (Stage)alert.getDialogPane().getScene().getWindow();
					stage.getIcons().add(new Image("file:icons/FreeVector-World-Logo.png"));
					alert.showAndWait();
					//TODO far disconnettere se c'� stato un errore
				}
				catch (RuntimeException e) {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Error in refresh");
					alert.setHeaderText(null);
					alert.setContentText(e.getMessage());
					final Stage stage;
					stage = (Stage)alert.getDialogPane().getScene().getWindow();
					stage.getIcons().add(new Image("file:icons/FreeVector-World-Logo.png"));
					alert.showAndWait();
				}
			}
		});

		disconnetti.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				try{
					cm.quit();
					loginshow = new Login();
					loginshow.start(primaryStage);
				}
				catch (RuntimeException e){
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Error");
					alert.setHeaderText(null);
					alert.setContentText(e.getMessage());
					final Stage stage;
					stage = (Stage)alert.getDialogPane().getScene().getWindow();
					stage.getIcons().add(new Image("file:icons/FreeVector-World-Logo.png"));
					alert.showAndWait();
				}
			}
		});

		esci.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Alert alert = new Alert(AlertType.CONFIRMATION);
				alert.setTitle("Confirm Exit");
				alert.setHeaderText(null);
				alert.setContentText("Are you sure you want to exit?");
				final Stage stage;
				stage = (Stage)alert.getDialogPane().getScene().getWindow();
				stage.getIcons().add(new Image("file:icons/FreeVector-World-Logo.png"));
				Optional<ButtonType> result = alert.showAndWait();
				if(result.get() == ButtonType.OK)
					System.exit(0);
			}
		});


		about.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {

				aboutPane = new StackPane();
				aboutStage = new Stage();
				aboutStage.setTitle("FTPSmart");
				VBox i = new VBox();
				info = new Label( "\nFTPSmart � un client ftp sviluppato per i sistemi operativi Windows e Linux"
						+ " \n  \n Sviluppato da: \n Balzani Silvia, Benini Francesco, Cecja Andia, Parra Stefano"
						+ "\n \t\t\t\t\t\t\t\t\t\t\t\t\t\t v 1.0.0 ");
				i.getChildren().add( info);
				i.setAlignment(Pos.BOTTOM_CENTER);

				Image logo = new Image("file:icons/FTPSMART.png",250,150,true,true);
				ImageView logoView = new ImageView();
				logoView.setImage(logo);
				VBox b = new VBox();
				b.getChildren().add(logoView);
				b.setAlignment(Pos.TOP_CENTER);

				aboutPane.getChildren().add(b);
				aboutPane.getChildren().add(i);
				aboutPane.setPadding(new Insets(25, 5, 25, 5));


				aboutPane.setStyle(
						"-fx-background-color: rgba(255, 255, 255, 0.25);" +
								"-fx-effect: dropshadow(gaussian, #5291EF, 70, 0, 0, 0);" +
								"-fx-background-insets: 10;"
						);

				aboutStage.setResizable(false);
				aboutStage.initStyle(StageStyle.TRANSPARENT);
				aboutStage.focusedProperty();

				Scene aboutscene = new Scene(aboutPane,450, 350);
				aboutscene.setFill(Color.TRANSPARENT);
				aboutStage.setScene(aboutscene);
				aboutStage.show();

				aboutscene.setOnMousePressed(new EventHandler<MouseEvent>() {

					@Override
					public void handle(MouseEvent event) {
						aboutStage.close();
					}

				});

			}

		});


		// Event Handler dei bottoni

		cancella.setOnMouseEntered(new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				cancella.setStyle("-fx-background-color: #eaeaea");
			};
		});

		cancella.setOnMouseExited(new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				cancella.setStyle("-fx-background-color: transparent");
			};
		});

		cancella.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Alert alert = new Alert(AlertType.CONFIRMATION);
				alert.setTitle("Delete File");
				alert.setHeaderText(null);
				alert.setContentText("Are you sure you want to delete this file?");
				final Stage stage;
				stage = (Stage)alert.getDialogPane().getScene().getWindow();
				stage.getIcons().add(new Image("file:icons/FreeVector-World-Logo.png"));
				Optional<ButtonType> result = alert.showAndWait();
				if(result.get() == ButtonType.OK){
					alert.close();
					try {
						cm.del();
						cm.refresh();
					} catch (ClassNotFoundException | IOException e ) {
						throw new RuntimeException("Error in removing the file.");

					}
				}
			}
		});

		carica.setOnMouseEntered(new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				carica.setStyle("-fx-background-color: #eaeaea");
			};
		});

		carica.setOnMouseExited(new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				carica.setStyle("-fx-background-color: transparent");
			};
		});

		carica.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Stage stage = new Stage();
				FileChooser fileChooser = new FileChooser();
				fileChooser.setTitle("Upload  File");
				File f = fileChooser.showOpenDialog(stage);
				if (f != null){
					if (f.isDirectory()){
						throw new RuntimeException("Not allowed to upload a directory.");
					}
					else{
						try {
							cm.stor(f);
						} catch (IOException e) {
							throw new RuntimeException("Error in stor");
						}
						try {
							cm.refresh();
						} catch (IOException | ClassNotFoundException e) {
							throw new RuntimeException("Error in refresh");
						}
					}

				}
			}
		});

		scarica.setOnMouseEntered(new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				scarica.setStyle("-fx-background-color: #eaeaea");
			};
		});

		scarica.setOnMouseExited(new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				scarica.setStyle("-fx-background-color: transparent");
			};
		});

		scarica.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Stage stage = new Stage();
				DirectoryChooser dir = new DirectoryChooser();
				dir.setTitle("Upload  File");
				File f =  dir.showDialog(stage);
				if (f != null){
					try {
						cm.retr(f);
					} catch (IOException e) {
						throw new RuntimeException("Error in retr");
					}
				}

			}

		});

		rinominaB.setOnMouseEntered(new EventHandler<Event>() {
			public void handle(Event event) {
				rinominaB.setStyle("-fx-background-color: #eaeaea");
			};
		});

		rinominaB.setOnMouseExited(new EventHandler<Event>() {
			public void handle(Event event){
				rinominaB.setStyle("-fx-background-color: transparent");
			};
		});


		aggiorna.setOnMouseEntered(new EventHandler<Event>() {
			public void handle(Event event) {
				aggiorna.setStyle("-fx-background-color: #eaeaea");
			};
		});

		aggiorna.setOnMouseExited(new EventHandler<Event>() {
			public void handle(Event event) {
				aggiorna.setStyle("-fx-background-color: transparent");
			};
		});
		aggiorna.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				try {
					cm.refresh();
				} catch (ClassNotFoundException | IOException  e) {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Error in refresh");
					alert.setHeaderText(null);
					alert.setContentText("Error in refresh. Please disconnect.");
					final Stage stage;
					stage = (Stage)alert.getDialogPane().getScene().getWindow();
					stage.getIcons().add(new Image("file:icons/FreeVector-World-Logo.png"));
					alert.showAndWait();
					//TODO far disconnettere se c'� stato un errore
				}
				catch (RuntimeException e) {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Error in refresh");
					alert.setHeaderText(null);
					alert.setContentText(e.getMessage());
					final Stage stage;
					stage = (Stage)alert.getDialogPane().getScene().getWindow();
					stage.getIcons().add(new Image("file:icons/FreeVector-World-Logo.png"));
					alert.showAndWait();
					//TODO far disconnettere se c'� stato un errore
				}
			}
		});

		nuovadir.setOnMouseEntered(new EventHandler<Event>() {
			public void handle(Event event) {
				nuovadir.setStyle("-fx-background-color: #eaeaea");
			};
		});

		nuovadir.setOnMouseExited(new EventHandler<Event>() {
			public void handle(Event event) {
				nuovadir.setStyle("-fx-background-color: transparent");
			};
		});

		nuovadir.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				TextInputDialog dialog = new TextInputDialog();
				dialog.setTitle("New Directory");
				dialog.setHeaderText(null);
				dialog.setContentText("Enter a new directory name");
				final Stage stage;
				stage = (Stage)dialog.getDialogPane().getScene().getWindow();
				stage.getIcons().add(new Image("file:icons/FreeVector-World-Logo.png"));
				Optional<String> result = dialog.showAndWait();
				String r = result.get(); // nome della cartella
				if (result.isPresent()){
					String p = FirstViewManager.getPath(FirstViewManager.getSelectedItem(albero));
					File f = new File(p);
					if (f.isDirectory())
						p = f.getParent();
					System.out.println(p);
					cm.newDir(p,r);
					try {
						cm.refresh();
					} catch (ClassNotFoundException | IOException | RuntimeException e) {
						throw new RuntimeException("Error in refresh.");
					}
				}
			}
		});



		rinominaB.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				TextInputDialog dialog = new TextInputDialog();
				dialog.setTitle("Rename");
				dialog.setHeaderText(null);
				dialog.setContentText("Enter new file name");
				final Stage stage;
				stage = (Stage)dialog.getDialogPane().getScene().getWindow();
				stage.getIcons().add(new Image("file:icons/FreeVector-World-Logo.png"));
				Optional<String> result = dialog.showAndWait();
				String r = result.get(); // nome della cartella
				if (result.isPresent()){
					cm.rename(FirstViewManager.getSelectedItem(albero), r);
					try {
						cm.refresh();
					} catch (ClassNotFoundException | IOException | RuntimeException e) {
						throw new RuntimeException("Error in rename.");
					}
				}
			}
		});


		percorsoServer = new Label("");

		// TextFields and its Preferred Width
		// percorsoField.setPrefWidth(400.0);
		///percorsoServerField = new TextField();

		/*
		 final Node rootIcon = new ImageView(new Image("file:icons/Computer.png",16,16,true,true));
		 final Node iconfolder = new ImageView(new Image("file:icons/folder.png",16,16,true,true));
		 final Node txt  = new ImageView(new Image("file:icons/txt.gif",16,16,true,true));
		 final Node pdf  = new ImageView(new Image("file:icons/pdf.png",16,16,true,true));
		 final Node html  = new ImageView(new Image("file:icons/HTML.png",16,16,true,true));
		 final Node img  = new ImageView(new Image("file:icons/image.png",16,16,true,true));
		 final Node servericon  = new ImageView(new Image("file:icons/server.gif",16,16,true,true));
		 */

		StackPane root = new StackPane();

		List<String> lista = new ArrayList<String>();
		ramo = FirstViewManager.createTree(null,lista);
		// dico che albero parte da qui
		albero = new TreeView<String>(ramo);
		try {
			cm.refresh();
		} catch (ClassNotFoundException | IOException | RuntimeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException("Error in refresh.");
		}

		albero.setEditable(false);

		//TODO rendere selezionato, modifica va fatto in un altro modo
		/*albero.setOnEditStart(new EventHandler<TreeView.EditEvent<String>>() {
			@Override
			public void handle(EditEvent<String> event) {

				TextInputDialog dialog = new TextInputDialog();
				dialog.setTitle("Rename file");
				dialog.setHeaderText(null);
				dialog.setContentText("Enter a new file name");
				final Stage stage;
				stage = (Stage)dialog.getDialogPane().getScene().getWindow();
				stage.getIcons().add(new Image("file:icons/FreeVector-World-Logo.png"));
				dialog.showAndWait();
			}
		});*/

		root.getChildren().add(albero);
		root.setAlignment(Pos.TOP_LEFT);


		percbox = new BorderPane();
		percbox.setLeft(percorsoServer);

		//Vbox server
		server = new BorderPane();
		server.setTop(percbox);
		server.setCenter(root);
		// server.getChildren().addAll(percbox,stack);
		// server.setPrefSize(600, 400);


		// Principal Pane Splitted
		grid = new BorderPane();
		mainLayout = new BorderPane();
		// mainLayout.setMaxSize(600, 300);
		// mainLayout.setOrientation(Orientation.VERTICAL);
		// mainLayout.setDividerPositions(0.05);
		mainLayout.setStyle("-fx-background-color: transparent;");
		// mainLayout.getItems().addAll(inizio,server);
		mainLayout.setTop(inizio);
		mainLayout.setCenter(server);

		//Add all to the grid layout
		grid.setCenter(mainLayout);

		//Show everything
		principalScene = new Scene(grid,500,500);
		this.primaryStage.setScene(principalScene);
		this.primaryStage.show();
	}
	
	public void disconnetti(){
		cm.quit();
		loginshow = new Login();
		loginshow.start(primaryStage);
	}

}


package com.ftpsmart.implementation;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javafx.application.Platform;
import javafx.scene.control.TreeItem;

import com.ftpsmart.implementation.*;
import com.ftpsmart.view.FirstView;

import org.apache.commons.lang3.SystemUtils;


public class CommandManager{
	public Socket scontrol; // per il controllo
	public FirstView fv;
	public Path currentPath;
	public InputStream in;
	public byte[] inputMessage = new byte[255];
	public OutputStream sout;
	public PrintWriter out;
	public String name;
	public BufferedInputStream bis;
	public FileInputStream fis;
	public OutputStream os;
	public FileOutputStream fos; 
	public TransfertThread tt;
	static public PairList pl = new PairList();
	static public List<Path> directoryList;


	public CommandManager(Socket s, String user, String passw, FirstView fv) throws RuntimeException {
		scontrol = s;
		this.fv = fv;
		createStream();
		initialization(user, passw);
	}

	public void createStream() throws RuntimeException{
		try {
			in = scontrol.getInputStream();
			sout = scontrol.getOutputStream();
			out = new PrintWriter(sout, true);
		}
		catch (IOException e){
			throw new RuntimeException("Client can't communicate with server.");
		}
	}


	public String receiveMessage() throws RuntimeException {
		try {
			String received;
			if (in.read(inputMessage) == -1){
				System.out.println("Error. It's not possible to communicate with server.");
				System.out.println("Closing.");
				throw new RuntimeException("Connection closed. Impossible communication with server.");
			}
			received = new String(inputMessage);
			received = received.trim();
			for (int i = 0; i < inputMessage.length; i++){
				inputMessage[i] = 0;
			}
			System.out.println("Receiving: " + received);
			return received;
		}
		catch (IOException e){
			Platform.runLater(new Runnable() {
				public void run() {
					try {
						fv.disconnetti();
					} catch (RuntimeException e) {
						throw e;
					}
				}
			});

		}
		return "";
	}

	void sendMessage(String message){
		System.out.println("Sending: " + message);
		out.println(message);
		out.flush();
	}


	public void newDir(String path, String dir) throws RuntimeException {
		sendMessage("cwd " + path);
		String received = receiveMessage();
		if (!(received.startsWith("2") || received.startsWith("3"))){
			throw new RuntimeException("This path is not avaible.");
		}
		sendMessage("mkd " + dir);
		received = receiveMessage();
		if (!(received.startsWith("2") || received.startsWith("3"))){
			throw new RuntimeException("Directory not allowed.");
		}
	}

	public void del() throws RuntimeException {
		sendMessage("cwd \\");
		String received = receiveMessage();
		if (!(received.startsWith("2") || received.startsWith("3"))){
			throw new RuntimeException("Error with the root directory.");
		}
		String dir = FirstViewManager.getPath(FirstViewManager.getSelectedItem(fv.albero));
		sendMessage("dele " + dir);
		received = receiveMessage();
		if (!(received.startsWith("2") || received.startsWith("3"))){
			throw new RuntimeException("Not allowed to remove this file.");
		}
	}

	public String findPort(String mex) {
		String[] c = mex.split(Pattern.quote(","));
		if (c.length != 6) return "";
		String ad;
		try {
			ad = new String (Integer.parseInt(c[0]) +".");
			ad = new String (ad + Integer.parseInt(c[1]) +".");
			ad = new String (ad + Integer.parseInt(c[2]) +".");
			ad = new String (ad + Integer.parseInt(c[3]));
			int porta = Integer.parseInt(c[4]) * 256 + Integer.parseInt(c[5]);
			return new String("" + porta);
		}
		catch(NumberFormatException e){
			e.printStackTrace();
			return new String("");
		}
	}


	public void port() throws RuntimeException {
		try {
			String received;
			tt = new TransfertThread(fv);
			sendMessage(" PORT " + tt.ssock.getLocalPort());
			received = receiveMessage();
			if (!(received.startsWith("2") || received.startsWith("3"))){
				throw new RuntimeException("This SocketServer port is not allowed");
			}
		} catch (IOException e) {
			throw new RuntimeException("I/O error occurred opening the ServerSocket.");
		}

	}


	public void rename(TreeItem<String> selezionato, String newName){
		String path = FirstViewManager.getPath(selezionato.getParent());
		sendMessage("cwd \\" + path);
		String received = receiveMessage();
		if (!(received.startsWith("2") || received.startsWith("3"))){
			throw new RuntimeException("Error in rename.");
		}
		String old = selezionato.getValue();
		sendMessage("RNFR " + old);
		received = receiveMessage();
		if (!(received.startsWith("2") || received.startsWith("3"))){
			throw new RuntimeException("Error in rename.");
		}
		sendMessage("RNTO " + newName);
		received = receiveMessage();
		if (!(received.startsWith("2") || received.startsWith("3"))){
			throw new RuntimeException("Error in rename.");
		}
	}

	public void initialization(String user, String pass) throws RuntimeException {
		String received = receiveMessage();
		if (!(received.startsWith("2") || received.startsWith("3"))){
			throw new RuntimeException("Client is not connected to server.");
		}
		sendMessage("USER " + user);
		received = receiveMessage();
		if (received.startsWith("2") || received.startsWith("3")){
			sendMessage("PASS " + pass);
			received = receiveMessage();
			if (!(received.startsWith("2") || received.startsWith("3"))){
				throw new RuntimeException("Login not successful.");
			}
		}
		else throw new RuntimeException("Login not successful.");
	}

	public void quit()  throws RuntimeException {
		sendMessage("QUIT");
	}


	public void refresh() throws IOException, RuntimeException, ClassNotFoundException {
		sendMessage("cwd \\");
		String received = receiveMessage();
		if (!(received.startsWith("2") || received.startsWith("3"))){
			throw new RuntimeException("Error in refresh.");
		}
		directoryList = new ArrayList<Path>();
		System.err.println("VUOTATA DIRECTORY LIST");
		fv.ramo.getChildren().remove(0, fv.ramo.getChildren().size());
		pl.add("", fv.ramo);
		Path p = Paths.get("");
		ls(p);	
	}

	public void ls(Path percorsoServer) throws IOException, RuntimeException, ClassNotFoundException{
		port();
		File f = generateTempFile(defaultFolder().toFile());
		tt.setTo("ls",f.toPath()); // deve salvare ls nel file temporaneo
		tt.start();
		sendMessage("LIST " + percorsoServer);
		String received = receiveMessage();
		if (received.startsWith("2") || received.startsWith("3")){
			System.out.println("Received list.");
		}
		else {
			removeFile(f.toPath());
			throw new RuntimeException("Something is not working in command LIST.");
		}

	}

	public void stor(File sorgente) throws IOException {
		//TODO da verificare se funziona
		port();
		boolean trovato = false;
		String cwd = null;
		TreeItem<String> sel = FirstViewManager.getSelectedItem(fv.albero);
		String cartella = FirstViewManager.getPath(sel);
		System.err.println("CARTELLA CERCATA: " + cartella);
		// cerca nella lista directory un cartella come quella
		for (Path p : directoryList){
			System.out.println("CARTELLA TROVATA: " + p.toString());
			if (p.toString().equals(cartella)){
				trovato = true;
				cwd = FirstViewManager.getPath(sel);
				break;
			}
		}
		if (!trovato){
			cwd = FirstViewManager.getPath(sel.getParent());
		}

		if (cwd == null) cwd = "\\";
		sendMessage("CWD " + cwd);
		String received = receiveMessage();
		if (received.startsWith("2") || received.startsWith("3")){
			System.out.println("Changed position.");
		}
		tt.setTo("stor",sorgente.toPath()); // il file che devo mandare 
		tt.start();
		sendMessage("STOR " + sorgente.getName()); // gli dico il file dove si trover�
		received = receiveMessage();
		if (received.startsWith("2") || received.startsWith("3")){
			System.out.println("Sent file.");
		}
		else {
			throw new RuntimeException("Error uploading.");
		}
	}

	public void retr(File f) throws IOException {
		//TODO e se quello sotto fosse nullo? Possibile
		//TODO da verificare se funziona
		port();
		TreeItem<String> sel = FirstViewManager.getSelectedItem(fv.albero);
		boolean trovato = false;
		String sorgente = FirstViewManager.getPath(sel);
		// cerca nella lista directory un cartella come quella
		for (Path p : directoryList){
			if (p.toString().equals(sorgente)){
				trovato = true;
				break;
			}
		}
		if (!trovato){
			tt.setTo("retr",Paths.get(f.toString() + File.separator + sel.getValue())); 
			tt.start();
			sendMessage("RETR " + sorgente);
			String received = receiveMessage();
			if (received.startsWith("2") || received.startsWith("3")){
				System.out.println("Received file.");
			}
			else {
				removeFile(f.toPath());
				throw new RuntimeException("Error downloading.");
			}
		}

	}

	public boolean renameFile(Path perc, String namefile, String newnamefile){   
		File old = new File(Paths.get(perc + "/" + namefile).toString());
		if(!old.exists()){
			System.out.println("File doesn't exist.");
			return false;   
		}   
		File new_file = new File(Paths.get(perc + "/" + newnamefile).toString());
		if (new_file.exists()) new_file.delete();
		old.renameTo(new_file);   
		return true;
	}

	//TODO implementare endretr e endstor

	public void endls(List<String> l, TreeItem<String> t) throws IOException, RuntimeException, ClassNotFoundException {
		FirstViewManager.createTree(t, l); // son
		pl.remove(0);
		if (!(pl.isEmpty())) ls(Paths.get(pl.getPath(0)));
	}

	public void endretr() throws RuntimeException {
		// ma se facessi partire una finestra?
		System.out.println("Sono nell'endretr.");
		try {
			refresh();
		} catch (ClassNotFoundException | IOException e) {
			throw new RuntimeException("Error in refresh");
		}
	}

	public void endstor(){
		// mma se facessi partire una finestra?
		System.out.println("Sono nell'endstor.");
	}	

	public static boolean removeFile(Path path){
		try {
			File file = new File(path.toString());
			File currentFile;
			if (file.exists()){
				String[] components = file.list();
				if (components != null){
					for(String s: components){
						currentFile = new File(file.getPath(),s);
						removeFile(currentFile.toPath());
					}
					Files.delete(file.toPath());
				}
				else
					Files.delete(file.toPath());
			}
			return true;
		}
		catch(Throwable e){
			System.err.println("Error in remove.");
			e.printStackTrace();
			return false;
		}
	}


	public static Path defaultFolder(){
		if (SystemUtils.IS_OS_WINDOWS){
			return Paths.get("C:/Users/"+ System.getProperty("user.name") + "/Documents/");
		}
		else {
			String user = System.getenv("SUDO_USER");
			if (user == null)
				user = System.getProperty("user.name");
			return Paths.get("/home/" + user);			
		}
	}

	static public File generateTempFile(File directory) throws IOException {
		File temp = File.createTempFile("tempfile", ".temp", directory); 
		System.out.println(temp.getAbsolutePath());
		return temp;
	}

	static public List<String> createList(File in) throws IOException{
		List<String> lista = new ArrayList<String>();
		BufferedReader br = new BufferedReader(new FileReader(in));
		String line;
		while ((line = br.readLine()) != null) {
			lista.add(line);
			// System.out.println(line);
		}
		br.close();
		return lista;
	}



}



package com.ftpsmart.implementation;

import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.TreeItem;

public class PairList {
	public List<Pair> lista = new ArrayList<Pair>();

	
	public void remove(int i){
		System.err.println("Rimossi nella pl: " + getPath(i) + " -- " + getTreeItem(i).getValue());
		lista.remove(lista.get(i));
	}

	public void add(String perc, TreeItem<String> par){
		Pair el = new Pair(perc,par);
		System.err.println("Aggiunti nella pl: " + perc + " -- " + par.getValue());
		lista.add(el);
	}

	public String getPath(Integer i){
		return lista.get(i).localPath;
	}

	public TreeItem<String> getTreeItem(Integer i){
		return lista.get(i).treeItem;
	}

	public void setPath(Integer i, String p){
		lista.get(i).localPath = p;
	}

	public void setTreeItem(Integer i, TreeItem<String> treeItem){
		lista.get(i).treeItem = treeItem;
	}
	
	public boolean isEmpty(){
		return lista.isEmpty();
	}
	
	public void printList(){
		System.err.println("Begin Print");
		for (Pair e : lista){
			System.err.println(e.treeItem.toString() + "--" + e.localPath);
		}
		System.err.println("End Print");
	}

}







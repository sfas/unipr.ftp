package com.ftpsmart.implementation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;



public class FirstViewManager {

	//TODO lista dei file su cui fare ricorsivamente ls

	// lista dei TreeItem collegati al nome... serve? 


	// prende l'oggetto selezionato nella TreeView e restituisce il suo path
	static public String getTreePath(TreeView<String> albero) {
		String path = File.separator;
		path = (String) ((TreeItem<String>)albero.getSelectionModel().getSelectedItem()).getValue();
		TreeItem<String> attuale = ((TreeItem<String>)albero.getSelectionModel().getSelectedItem());
		while(attuale != null){
			attuale = attuale.getParent();
			if (attuale != null)
				path = attuale.getValue() + File.separator + path;
		}
		path = File.separator + path;
		return path;
	}


	static public List<String> generateList(File temp){
		List<String> lista = new ArrayList<String>();
		String linea;
		BufferedReader bf = null;
		try {
			FileReader fr = new FileReader(temp);
			bf = new BufferedReader(fr);
			while (true){
				linea = bf.readLine();
				linea = linea.trim();
				if (linea.equals(null)) break;
				lista.add(linea);
			}
		}
		catch(Throwable e){
		}
		finally {
			try{
				bf.close();
			}
			catch(Throwable e)
			{}
		}
		return lista;
	}

	// restituisce il TreeItem selezionato
	static public TreeItem<String> getSelectedItem(TreeView<String> albero) {
		return ((TreeItem<String>)albero.getSelectionModel().getSelectedItem());
	}

	// dato un TreeItem risponde con il path che avr� sul server
	static public String getPath(TreeItem<String> attuale){
		try {
			String path = attuale.getValue();
			while(attuale != null){
				attuale = attuale.getParent();
				if (attuale != null)
					path = attuale.getValue() + File.separator + path;
			}
			path = File.separator + path;
			path = removeServerFromPath(path);
			return path;
		}
		catch (NullPointerException e){
			throw new RuntimeException("No file to delete.");
		}
	}


	static public String removeServerFromPath(String path){
		// toglie 8 perch� tutti iniziano con "Server\"
		if (path.equals("\\Server"))
			return "";
		else
		{
			path = path.substring(8);
			return path;
		}
	}
	
	
	static ImageView getImage(String tit){
		if (tit.endsWith(".txt"))
			return new ImageView(new Image("file:icons/txt.gif",16,16,true,true));
		if (tit.endsWith(".pdf"))
			return new ImageView(new Image("file:icons/pdf.png",16,16,true,true));
		if (tit.endsWith(".html"))
			return new ImageView(new Image("file:icons/HTML.png",16,16,true,true));
		if (tit.endsWith(".htm"))
			return new ImageView(new Image("file:icons/HTML.png",16,16,true,true));
		if (tit.endsWith(".jpg"))
			return new ImageView(new Image("file:icons/image.png",16,16,true,true));
		if (tit.endsWith(".jpeg"))
			return new ImageView(new Image("file:icons/image.png",16,16,true,true));		
		if (tit.endsWith(".png"))
			return new ImageView(new Image("file:icons/image.png",16,16,true,true));
		if (tit.endsWith(".gif"))
			return new ImageView(new Image("file:icons/image.png",16,16,true,true));
		return new ImageView(new Image("file:icons/txt.gif",16,16,true,true));
	}

	// Aggiunge al TreeItem gli elementi contenuti nella lista
	// inoltre aggiunge a pl le cartelle contenute, cos� le pu� visitare
	static public TreeItem<String> createTree(TreeItem<String> tree, List<String> lista){
		File f;
		if (tree == null){
			TreeItem<String> server = new TreeItem<String> ("Server",new ImageView(new Image("file:icons/server.gif",16,16,true,true)));
			server.setExpanded(true);
			for (String elemento : lista){
				if (elemento.contains("\\") || elemento.contains("/")){
					elemento = elemento.substring(0, elemento.length() - 1);
					TreeItem<String> elem = new TreeItem<String>(elemento, new ImageView(new Image("file:icons/folder.png",16,16,true,true)));
					server.getChildren().add(elem);
					// elem.setExpanded(true);
					elemento = getPath(elem);
					CommandManager.directoryList.add(Paths.get(elemento));
					CommandManager.pl.lista.add(new Pair(elemento,elem));

				}
				else {
					TreeItem<String> elem = new TreeItem<String>(elemento, getImage(elemento));
					server.getChildren().add(elem);
				}
			}
			// TreeView<String> albero = new TreeView<String>(server);
			return server;
		}
		else {
			for (String elemento : lista){
				if (elemento.contains("\\") || elemento.contains("/")){
					elemento = elemento.substring(0, elemento.length() - 1);
					TreeItem<String> elem = new TreeItem<String>(elemento, new ImageView(new Image("file:icons/folder.png",16,16,true,true)));
					tree.getChildren().add(elem);
					elemento = getPath(elem);
					// elem.setExpanded(true)
					CommandManager.directoryList.add(Paths.get(elemento));;
					CommandManager.pl.add(elemento, elem);
				}
				else {
					TreeItem<String> elem = new TreeItem<String>(elemento, getImage(elemento));
					tree.getChildren().add(elem);
				}
			}
			return tree;
		}
	}


}

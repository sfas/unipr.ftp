package com.ftpsmart.implementation;

import javafx.scene.control.TreeItem;

public class Pair {
	public String localPath; 
	public TreeItem<String> treeItem;  

	public Pair(String p, TreeItem<String> i){
		this.localPath = p;
		this.treeItem = i;
	}
}

